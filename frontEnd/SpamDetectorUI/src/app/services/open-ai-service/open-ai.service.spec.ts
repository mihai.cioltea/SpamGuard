import { TestBed } from '@angular/core/testing';
import { OpenAIService } from './open-ai.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RevengeModel } from '../../models/open-ai-revenge/revengeModel';

describe('OpenAIService', () => {
  let service: OpenAIService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ OpenAIService ]
    }); 
    service = TestBed.inject(OpenAIService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('generateConversation', () => {
    it('should generate a conversation successfully', () => {
      const mockResponse = 'Generated conversation text';

      service.generateConversation().subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/OpenAIRevenge/generateConversation`);
      expect(req.request.method).toBe('GET');
      expect(req.request.responseType).toBe('text');
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      service.generateConversation().subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/OpenAIRevenge/generateConversation`);
      expect(req.request.method).toBe('GET');
      expect(req.request.responseType).toBe('text');
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('removeHasRevenge', () => {
    it('should remove has revenge successfully', () => {
      const mockResponse = { success: true };
      const userEmail = 'test@example.com';

      service.removeHasRevenge(userEmail).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/OpenAIRevenge/removeHasRevenge`);
      expect(req.request.method).toBe('POST');
      expect(req.request.headers.get('Content-Type')).toBe('application/json');
      expect(req.request.body).toBe(`"${userEmail}"`);
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const userEmail = 'test@example.com';

      service.removeHasRevenge(userEmail).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/OpenAIRevenge/removeHasRevenge`);
      expect(req.request.method).toBe('POST');
      expect(req.request.headers.get('Content-Type')).toBe('application/json');
      expect(req.request.body).toBe(`"${userEmail}"`);
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('sendAttack', () => {
    it('should send attack successfully', () => {
      const mockResponse = { success: true };
      const revengeModel: RevengeModel = {
        UserEmail: 'foo@example.com',
        AttackerEmail: 'attacker@example.com',
        GeneratedEmail: 'This is a test attack.'
      };

      service.sendAttack(revengeModel).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/OpenAIRevenge/sendAttack`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(revengeModel);
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const revengeModel: RevengeModel = {
        UserEmail: 'foo@example.com',
        AttackerEmail: 'attacker@example.com',
        GeneratedEmail: 'This is a test attack.'
      };

      service.sendAttack(revengeModel).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/OpenAIRevenge/sendAttack`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(revengeModel);
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });
});
