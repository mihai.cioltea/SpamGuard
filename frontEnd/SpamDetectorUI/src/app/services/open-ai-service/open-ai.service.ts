import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RevengeModel } from '../../models/open-ai-revenge/revengeModel';

@Injectable({
  providedIn: 'root'
})
export class OpenAIService {
  private readonly baseOpenAIUrl: string =  'https://localhost:7062/api/OpenAIRevenge/';

  constructor(private httpClient : HttpClient) { }

  public generateConversation() : Observable<string> {
    return this.httpClient.get(
      this.baseOpenAIUrl+'generateConversation',
      {responseType: 'text'}
    );
  }
  
  public removeHasRevenge(userEmail: string) : Observable<any> {
    return this.httpClient.post(
      `${this.baseOpenAIUrl}removeHasRevenge`,
      `"${userEmail}"`,
      { headers: { 'Content-Type': 'application/json' } }
    );
  }

  public sendAttack(revengeModel: RevengeModel) : Observable<any> {
    return this.httpClient.post(
      this.baseOpenAIUrl+'sendAttack',
      revengeModel
    );
  }
}
