import { TestBed } from '@angular/core/testing';
import { PaymentService } from './payment.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('PaymentService', () => {
  let service: PaymentService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ PaymentService ]
    });
    service = TestBed.inject(PaymentService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('requestMemberSession', () => {
    it('should send request for member session successfully', () => {
      const priceId = 'price_123';
      const mockResponse = { sessionId: 'session_123' };

      service.requestMemberSession(priceId).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/Stripe/create-checkout-session`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual({
        priceId: 'price_123',
        successUrl: 'http://localhost:4200/main-page',
        failureUrl: 'http://localhost:4200/main-page'
      });
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const priceId = 'price_123';

      service.requestMemberSession(priceId).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/Stripe/create-checkout-session`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual({
        priceId: 'price_123',
        successUrl: 'http://localhost:4200/main-page',
        failureUrl: 'http://localhost:4200/main-page'
      });
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('sessionDetails', () => {
    it('should retrieve session details successfully', () => {
      const sessionId = 'session_123';
      const userEmail = 'test@example.com';
      const priceId = 'price_123';
      const mockResponse = 'Session details';

      service.sessionDetails(sessionId, userEmail, priceId).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/Stripe/session-details`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual({
        sessionId: sessionId,
        userEmail: userEmail,
        priceId: priceId
      });
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const sessionId = 'session_123';
      const userEmail = 'test@example.com';
      const priceId = 'price_123';

      service.sessionDetails(sessionId, userEmail, priceId).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/Stripe/session-details`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual({
        sessionId: sessionId,
        userEmail: userEmail,
        priceId: priceId
      });
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });
});
