import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ISession } from '../../models/payment/ISession';
import { Observable} from 'rxjs';

@Injectable()
export class PaymentService {
  private readonly basePaymentUrl: string =  'https://localhost:7062/api/Stripe/';
  private readonly Url: string = 'http://localhost:4200/main-page';
  public readonly CHECKTOKENPRICEID: string = 'price_1PCKelEGqvAYGSliBQ8tEbzj';
  public readonly REVENGETOKENPRICEID: string = 'price_1PD3V0EGqvAYGSli5ZsKe4cH';
  public readonly PublicStripeKey: string = 'pk_test_51PBzy6EGqvAYGSlixwy0Y0wsLdv4UNjXfAUKQVtjorst9Hprkj0FrgtI6xDDwK1zXgUFem48RpYVCwbRcCMhjsOK00DvWnkz9P';
  
  constructor(private httpClient : HttpClient) { }

  public requestMemberSession(priceId: string): Observable<any> {
    return this.httpClient.post<ISession>(
      this.basePaymentUrl + 'create-checkout-session', 
      {
        priceId: priceId,
        successUrl: this.Url,
        failureUrl: this.Url,
      });
  }

  public sessionDetails(sessionId: string, userEmail: string, priceId: string): Observable<string> {
    return this.httpClient.post<string>(
      this.basePaymentUrl +'session-details', 
      {
          sessionId: sessionId,
          userEmail: userEmail,
          priceId: priceId
      });
  } 
}
