import { Injectable }     from '@angular/core';
import { CanActivate }    from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate() {
        const authToken = sessionStorage.getItem('authToken');
        if (authToken == null) {
         this.router.navigate(['home-screen']);
        }
        return true;
    }
    constructor(private router: Router) { }
}