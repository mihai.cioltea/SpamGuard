import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserRegister } from '../../models/user-management/userRegister';
import { Observable, catchError, of, take } from 'rxjs';
import { User } from '../../models/user-management/user';
import { UserLogin } from '../../models/user-management/userLogin';
import { UserPasswordReset } from '../../models/user-management/userResetPassword';
import { UserResponse } from '../../models/user-management/userResponse';
import { ActionToken } from '../../models/user-management/actionToken';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private readonly baseAuthUrl: string =  'https://localhost:7062/api/Auth/';

  constructor(private httpClient: HttpClient) { }

  public getUserInfo(userEmail: string) : Observable<string> {
    return this.httpClient.get(
      this.baseAuthUrl+'user-info?userEmail='+userEmail,
      {responseType: 'text'}
    );
  }

  public getActionToken(userEmail: string) : Observable<ActionToken> {
    return this.httpClient.get<ActionToken>(
      this.baseAuthUrl+'action-token?userEmail='+userEmail
    );
  }

  public register(user: UserRegister) : Observable<User> {
    return this.httpClient.post<User>(
      this.baseAuthUrl+'register',
      user
    );
  }

  public login(user: UserLogin) : Observable<UserResponse> {
    return this.httpClient.post<UserResponse>(
      this.baseAuthUrl+'login',
      user
    );
  }

  public forgotPassword(email: string) : Observable<any> {
    const params = { email: email };
    return this.httpClient.post(
      this.baseAuthUrl+'forgot-password',
      {},
      { params }
    );
  }

  public resetPassword(user: UserPasswordReset) : Observable<any> {
    return this.httpClient.post<UserPasswordReset>(
      this.baseAuthUrl+'reset-password',
      user
    );
  }

  public refreshToken(user: UserLogin) : Observable<any> {
    return this.httpClient.post(
      this.baseAuthUrl+'refresh-token',
      user
    );
  }

  public async checkServerStatus(): Promise<boolean> {
    try {
      const resp: any = await this.httpClient.get(this.baseAuthUrl + 'check-server-status').pipe(
        take(1),
        catchError(() => of({ status: 500 }))
      ).toPromise();

      return resp.status === 200;
    } catch (error) {
      return false;
    }
  }

  public removeRefreshToken(userEmail: string) : Observable<any> {
    const params = { userEmail: userEmail };
    return this.httpClient.delete(
      this.baseAuthUrl+'remove-refreshToken',
      { params }
    );
  }
}
