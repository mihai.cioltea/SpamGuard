import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActionToken } from '../../models/user-management/actionToken';
import { UserRegister } from '../../models/user-management/userRegister';
import { User } from '../../models/user-management/user';
import { UserLogin } from '../../models/user-management/userLogin';
import { UserResponse } from '../../models/user-management/userResponse';
import { UserPasswordReset } from '../../models/user-management/userResetPassword';
import { HttpErrorResponse } from '@angular/common/http';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ AuthService ]
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getUserInfo', () => {
    it('should retrieve user info', () => {
      const userEmail = 'test@example.com';
      const dummyResponse = 'User Info';
      
      service.getUserInfo(userEmail).subscribe(response => {
        expect(response).toEqual(dummyResponse);
      });
      
      const req = httpMock.expectOne(`https://localhost:7062/api/Auth/user-info?userEmail=${userEmail}`);
      expect(req.request.method).toBe('GET');
      req.flush(dummyResponse);
    });

    it('should handle error while retrieving user info', () => {
      const userEmail = 'test@example.com';
      const errorMessage = 'Error occurred';
      
      service.getUserInfo(userEmail).subscribe(
        response => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
      
      const req = httpMock.expectOne(`https://localhost:7062/api/Auth/user-info?userEmail=${userEmail}`);
      expect(req.request.method).toBe('GET');
      req.error(new ErrorEvent(errorMessage));
    });
  });

  describe('getActionToken', () => {
    it('should retrieve action token', () => {
      const userEmail = 'test@example.com';
      const dummyToken: ActionToken = new ActionToken();
      
      service.getActionToken(userEmail).subscribe(token => {
        expect(token).toEqual(dummyToken);
      });
      
      const req = httpMock.expectOne(`https://localhost:7062/api/Auth/action-token?userEmail=${userEmail}`);
      expect(req.request.method).toBe('GET');
      req.flush(dummyToken);
    });

    it('should handle error while retrieving action token', () => {
      const userEmail = 'test@example.com';
      const errorMessage = 'Error occurred';
      
      service.getActionToken(userEmail).subscribe(
        token => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
      
      const req = httpMock.expectOne(`https://localhost:7062/api/Auth/action-token?userEmail=${userEmail}`);
      expect(req.request.method).toBe('GET');
      req.error(new ErrorEvent(errorMessage));
    });
  });
  
  describe('register', () => {
    it('should register user', () => {
      const userRegisterData: UserRegister = new UserRegister();
      const dummyUser: User = new User();

      service.register(userRegisterData).subscribe(user => {
        expect(user).toEqual(dummyUser);
      });
      
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/register');
      expect(req.request.method).toBe('POST');
      req.flush(dummyUser);
    });

    it('should handle error during user registration', () => {
      const userRegisterData: UserRegister = new UserRegister();
      const errorMessage = 'Error occurred';

      service.register(userRegisterData).subscribe(
        user => {},
        error => {
          expect(error).toBeTruthy();
        }
      );

      const req = httpMock.expectOne('https://localhost:7062/api/Auth/register');
      expect(req.request.method).toBe('POST');
      req.error(new ErrorEvent(errorMessage));
    });
  });

  describe('login', () => {
    it('should log in user', () => {
      const userLoginData: UserLogin = new UserLogin();
      const dummyUserResponse: UserResponse = new UserResponse();
  
      service.login(userLoginData).subscribe(userResponse => {
        expect(userResponse).toEqual(dummyUserResponse);
      });
      
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/login');
      expect(req.request.method).toBe('POST');
      req.flush(dummyUserResponse);
    });
  
    it('should handle error during user login', () => {
      const userLoginData: UserLogin = new UserLogin();
      const errorMessage = 'Error occurred';
  
      service.login(userLoginData).subscribe(
        userResponse => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/login');
      expect(req.request.method).toBe('POST');
      req.error(new ErrorEvent(errorMessage));
    });
  });
  
  describe('forgotPassword', () => {
    it('should request forgot password', () => {
      const email = 'test@example.com';
  
      service.forgotPassword(email).subscribe();
      
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/forgot-password?email=test@example.com');
      expect(req.request.method).toBe('POST');
      req.flush({});
    });
  
    it('should handle error during forgot password request', () => {
      const email = 'test@example.com';
      const errorMessage = 'Error occurred';
  
      service.forgotPassword(email).subscribe(
        response => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/forgot-password?email=test@example.com');
      expect(req.request.method).toBe('POST');
      req.error(new ErrorEvent(errorMessage));
    });
  });

  describe('resetPassword', () => {
    it('should reset password', () => {
      const userResetData: UserPasswordReset = new UserPasswordReset();
  
      service.resetPassword(userResetData).subscribe();
      
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/reset-password');
      expect(req.request.method).toBe('POST');
      req.flush({});
    });
  
    it('should handle error during password reset', () => {
      const userResetData: UserPasswordReset = new UserPasswordReset();
      const errorMessage = 'Error occurred';
  
      service.resetPassword(userResetData).subscribe(
        response => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/reset-password');
      expect(req.request.method).toBe('POST');
      req.error(new ErrorEvent(errorMessage));
    });
  });

  describe('refreshToken', () => {
    it('should add refresh token', () => {
      const userLoginData: UserLogin = new UserLogin();
  
      service.refreshToken(userLoginData).subscribe();
      
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/refresh-token');
      expect(req.request.method).toBe('POST');
      req.flush({});
    });
  
    it('should handle error during adding the token refresh', () => {
      const userLoginData: UserLogin = new UserLogin();
      const errorMessage = 'Error occurred';
  
      service.refreshToken(userLoginData).subscribe(
        response => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/refresh-token');
      expect(req.request.method).toBe('POST');
      req.error(new ErrorEvent(errorMessage));
    });
  });

  describe('checkServerStatus', () => {
    it('should return true if server is up', fakeAsync(() => {
      let serverStatus: boolean | undefined;
  
      service.checkServerStatus().then(status => {
        serverStatus = status;
      });
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/check-server-status');
      expect(req.request.method).toBe('GET');
      req.flush({ status: 200 });
  
      tick();
  
      expect(serverStatus).toBe(true);
    }));
  
    it('should return false if server is down', fakeAsync(() => {
      let serverStatus: boolean | undefined;
  
      service.checkServerStatus().then(status => {
        serverStatus = status;
      });
  
      const errorMessage = 'Server error';
      const errorResponse = new HttpErrorResponse({
        error: errorMessage,
        status: 500,
        statusText: 'Internal Server Error'
      });
  
      const errorEvent = new ErrorEvent('error', {
        error: errorResponse
      });
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/check-server-status');
      expect(req.request.method).toBe('GET');
      req.error(errorEvent);
  
      tick();
  
      expect(serverStatus).toBe(false);
    }));
  });

  describe('removeRefreshToken', () => {
    it('should remove refresh token', () => {
      const userEmail = 'test@example.com';
  
      service.removeRefreshToken(userEmail).subscribe();
      
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/remove-refreshToken?userEmail=test@example.com');
      expect(req.request.method).toBe('DELETE');
      req.flush({});
    });
  
    it('should handle error during token removal', () => {
      const userEmail = 'test@example.com';
      const errorMessage = 'Error occurred';
  
      service.removeRefreshToken(userEmail).subscribe(
        response => {},
        error => {
          expect(error).toBeTruthy();
        }
      );
  
      const req = httpMock.expectOne('https://localhost:7062/api/Auth/remove-refreshToken?userEmail=test@example.com');
      expect(req.request.method).toBe('DELETE');
      req.error(new ErrorEvent(errorMessage));
    });
  });
});
