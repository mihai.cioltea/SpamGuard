import { Injectable } from '@angular/core';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IGoogleUser } from '../../models/user-management/IGoogleUser';

const oAuthConfig: AuthConfig = {
  issuer: 'https://accounts.google.com',
  strictDiscoveryDocumentValidation: false,
  redirectUri: window.location.origin + '/main-page',
  clientId: '26885951461-icnjdmc9fulmdrtg0u4bva1eif6e4qe0.apps.googleusercontent.com',
  scope: 'openid profile email https://mail.google.com/',
  timeoutFactor: 5000
}
const gmailApi = 'https://gmail.googleapis.com';

@Injectable()
export class GmailapiserviceService {

  constructor(private oAuthService: OAuthService,
    private httpClient: HttpClient) {}

  public logIntoMailProvider(): void {
    this.oAuthService.configure(oAuthConfig);
    this.oAuthService.loadDiscoveryDocument().then(() => {
      this.oAuthService.tryLoginImplicitFlow().then(() => {
        if(!this.oAuthService.hasValidAccessToken()) {
          this.oAuthService.initLoginFlow();
        } 
        else {
          this.oAuthService.loadUserProfile().then( (profile) => {
            sessionStorage.setItem('userProfile', (profile as IGoogleUser).info.sub);
          });
        }
      });
    });
  }

  public signOut(): void {
    this.oAuthService.logOut();
  }

  public isLoggedIn(): boolean {
    return this.oAuthService.hasValidAccessToken();
  }

  public emails(userId: string, numberOfEmails: number): Observable<any> {
    return this.httpClient.get(
      `${gmailApi}/gmail/v1/users/${userId}/messages`,
      { 
        headers: this.authHeader(),
        params: {maxResults: numberOfEmails}
      });
  }

  public getMail(userId: string, mailId : string): Observable<any> {
    return this.httpClient.get(
      `${gmailApi}/gmail/v1/users/${userId}/messages/${mailId}`,
      { 
        headers: this.authHeader(), 
        params: new HttpParams().set('format', 'full')
      });
  }

  private authHeader(): HttpHeaders {
    return new HttpHeaders({
      'Authorization': `Bearer ${this.oAuthService.getAccessToken()}`
    });
  }
}
