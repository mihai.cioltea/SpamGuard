import { TestBed } from '@angular/core/testing';
import { GmailapiserviceService } from './gmailapiservice.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { OAuthService, OAuthSuccessEvent } from 'angular-oauth2-oidc';

class MockOAuthService {
  configure() {}
  loadDiscoveryDocument() { return Promise.resolve({} as OAuthSuccessEvent); }
  tryLoginImplicitFlow() { return Promise.resolve(); }
  hasValidAccessToken() { return false; }
  initLoginFlow() {}
  logOut() {}
  loadUserProfile() { return Promise.resolve({}); }
  getAccessToken() { return ''; }
}

describe('GmailapiserviceService', () => {
  let service: GmailapiserviceService;
  let oauthService: OAuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ GmailapiserviceService,
        { provide: OAuthService, useClass: MockOAuthService } ]
    });
    service = TestBed.inject(GmailapiserviceService);
    oauthService = TestBed.inject(OAuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('logIntoMailProvider', () => {
    it('should log into mail provider', async () => {
      const configureSpy = spyOn(oauthService, 'configure');
      const loadDiscoveryDocumentSpy = spyOn(oauthService, 'loadDiscoveryDocument').and.returnValue(Promise.resolve({} as OAuthSuccessEvent));
      const tryLoginImplicitFlowSpy = spyOn(oauthService, 'tryLoginImplicitFlow').and.returnValue(Promise.resolve(true));
    
      await service.logIntoMailProvider();
    
      expect(configureSpy).toHaveBeenCalled();
      expect(loadDiscoveryDocumentSpy).toHaveBeenCalled();
      expect(tryLoginImplicitFlowSpy).toHaveBeenCalled();
    });
  });

  describe('signOut', () => {
    it('should sign out', () => {
      const logOutSpy = spyOn(oauthService, 'logOut');
  
      service.signOut();
  
      expect(logOutSpy).toHaveBeenCalled();
    });
  });

  describe('isLoggedIn', () => {
    it('should check if user is logged in', () => {
      const hasValidAccessTokenSpy = spyOn(oauthService, 'hasValidAccessToken').and.returnValue(true);
  
      const isLoggedIn = service.isLoggedIn();
  
      expect(hasValidAccessTokenSpy).toHaveBeenCalled();
      expect(isLoggedIn).toBeTruthy();
    });
  });

  describe('emails', () => {
    it('should retrieve emails', () => {
      const userId = 'example_user';
      const numberOfEmails = 10;
      const gmailApi = 'https://gmail.googleapis.com';
  
      service.emails(userId, numberOfEmails).subscribe();
  
      const req = httpMock.expectOne(`${gmailApi}/gmail/v1/users/${userId}/messages?maxResults=${numberOfEmails}`);
      expect(req.request.method).toBe('GET');
      expect(req.request.headers.get('Authorization')).toBeTruthy();
    });
  });

  describe('getMail', () => {
    it('should retrieve a single mail', () => {
      const userId = 'example_user';
      const mailId = 'example_mail_id';
      const gmailApi = 'https://gmail.googleapis.com';
  
      service.getMail(userId, mailId).subscribe();
  
      const req = httpMock.expectOne(`${gmailApi}/gmail/v1/users/${userId}/messages/${mailId}?format=full`);
      expect(req.request.method).toBe('GET');
      expect(req.request.headers.get('Authorization')).toBeTruthy();
    });
  });
});
