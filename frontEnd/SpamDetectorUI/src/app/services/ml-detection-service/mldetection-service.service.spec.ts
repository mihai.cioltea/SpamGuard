import { TestBed } from '@angular/core/testing';
import { MLDetectionServiceService } from './mldetection-service.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PredictionRequestModelWithUser } from '../../models/ml-detection-attacks/PredictionRequestModelWithUser';
import { ModelOutput } from '../../models/ml-detection-attacks/modelOutput';
import { SaveAttackRequestModel } from '../../models/ml-detection-attacks/SaveAttackRequestModel';
import { AttackPeriod } from '../../models/ml-detection-attacks/attackPeriod';
import { AttackPeriodV2 } from '../../models/ml-detection-attacks/attackPeriodV2';
import { FeedBackModel } from '../../models/ml-detection-attacks/feedbackModel';

describe('MLDetectionServiceService', () => {
  let service: MLDetectionServiceService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ MLDetectionServiceService ]
    });
    service = TestBed.inject(MLDetectionServiceService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('messagePrediction', () => {
    it('should return ModelOutput when called with valid data', () => {
      const request: PredictionRequestModelWithUser = new PredictionRequestModelWithUser();
      const response: ModelOutput = new ModelOutput();

      service.messagePrediction(request).subscribe(res => {
        expect(res).toEqual(response);
      });

      const req = httpMock.expectOne('https://localhost:7062/api/MLDetection/messagePrediction');
      expect(req.request.method).toBe('POST');
      req.flush(response);
    });

    it('should handle error response', () => {
      const request: PredictionRequestModelWithUser = new PredictionRequestModelWithUser();

      service.messagePrediction(request).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne('https://localhost:7062/api/MLDetection/messagePrediction');
      expect(req.request.method).toBe('POST');
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('trainModel', () => {
    it('should return true when called successfully', () => {
      const response = true;

      service.trainModel().subscribe(res => {
        expect(res).toEqual(response);
      });

      const req = httpMock.expectOne('https://localhost:7062/api/MLDetection/trainModel');
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    it('should handle error response', () => {
      service.trainModel().subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne('https://localhost:7062/api/MLDetection/trainModel');
      expect(req.request.method).toBe('GET');
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('saveAttack', () => {
    it('should save attack successfully', () => {
      const request: SaveAttackRequestModel = new SaveAttackRequestModel();
      const response = { success: true };

      service.saveAttack(request).subscribe(res => {
        expect(res).toEqual(response);
      });

      const req = httpMock.expectOne('https://localhost:7062/api/MLDetection/saveAttack');
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(request);
      req.flush(response);
    });

    it('should handle error response', () => {
      const request: SaveAttackRequestModel = new SaveAttackRequestModel();

      service.saveAttack(request).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne('https://localhost:7062/api/MLDetection/saveAttack');
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toEqual(request);
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('attackPeriods', () => {
    it('should fetch attack periods successfully', () => {
      const request = 'test@example.com';
      const mockResponse: AttackPeriod[] = [];

      service.attackPeriods(request).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/attackPeriods?email=${request}`);
      expect(req.request.method).toBe('GET');
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const request = 'test@example.com';

      service.attackPeriods(request).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/attackPeriods?email=${request}`);
      expect(req.request.method).toBe('GET');
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('biggestThreath', () => {
    it('should fetch biggest threats successfully', () => {
      const request = 'test@example.com';
      const mockResponse: AttackPeriodV2[] = [];

      service.biggestThreath(request).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/biggestThreath?email=${request}`);
      expect(req.request.method).toBe('GET');
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const request = 'test@example.com';

      service.biggestThreath(request).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/biggestThreath?email=${request}`);
      expect(req.request.method).toBe('GET');
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('addFeedback', () => {
    it('should send feedback successfully', () => {
      const feedBackModelDto: FeedBackModel = { IsOK: true, Message: 'This is feedback' };
      const mockResponse = { message: 'Feedback received' };

      service.addFeedback(feedBackModelDto).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/addFeedback`);
      expect(req.request.method).toBe('PUT');
      expect(req.request.body).toEqual(feedBackModelDto);
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const feedBackModelDto: FeedBackModel = { IsOK: true, Message: 'This is feedback' };

      service.addFeedback(feedBackModelDto).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/addFeedback`);
      expect(req.request.method).toBe('PUT');
      expect(req.request.body).toEqual(feedBackModelDto);
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

  describe('getFeedback', () => {
    it('should retrieve feedback successfully', () => {
      const message = 'Sample feedback message';
      const mockResponse = { feedback: 'Positive' };

      service.getFeedback(message).subscribe(res => {
        expect(res).toEqual(mockResponse);
      });

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/getFeedback`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toBe(`"${message}"`);
      expect(req.request.headers.get('Content-Type')).toBe('application/json');
      req.flush(mockResponse);
    });

    it('should handle error response', () => {
      const message = 'Sample feedback message';

      service.getFeedback(message).subscribe(
        res => fail('should have failed with the error'),
        error => {
          expect(error.status).toBe(500);
        }
      );

      const req = httpMock.expectOne(`https://localhost:7062/api/MLDetection/getFeedback`);
      expect(req.request.method).toBe('POST');
      expect(req.request.body).toBe(`"${message}"`);
      expect(req.request.headers.get('Content-Type')).toBe('application/json');
      req.flush('Something went wrong', {
        status: 500,
        statusText: 'Server Error'
      });
    });
  });

});
