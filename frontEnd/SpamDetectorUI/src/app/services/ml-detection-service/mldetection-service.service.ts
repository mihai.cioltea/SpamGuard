import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AttackPeriod } from '../../models/ml-detection-attacks/attackPeriod';
import { AttackPeriodV2 } from '../../models/ml-detection-attacks/attackPeriodV2';
import { SaveAttackRequestModel } from '../../models/ml-detection-attacks/SaveAttackRequestModel';
import { PredictionRequestModelWithUser } from '../../models/ml-detection-attacks/PredictionRequestModelWithUser';
import { ModelOutput } from '../../models/ml-detection-attacks/modelOutput';
import { FeedBackModel } from '../../models/ml-detection-attacks/feedbackModel';

@Injectable({
  providedIn: 'root'
})
export class MLDetectionServiceService {
  private readonly baseDectionUrl: string =  'https://localhost:7062/api/MLDetection/';

  constructor(private httpClient: HttpClient) { }

  public messagePrediction(request : PredictionRequestModelWithUser) : Observable<any> {
    return this.httpClient.post<ModelOutput>(
      this.baseDectionUrl+'messagePrediction',
      request
    );
  }

  public trainModel() : Observable<boolean> {
    return this.httpClient.get<boolean>(
      this.baseDectionUrl+'trainModel',
    );
  }

  public saveAttack(request: SaveAttackRequestModel) : Observable<any> {
    return this.httpClient.post(
      this.baseDectionUrl+'saveAttack',
      request
    );
  }

  public attackPeriods(request: string) : Observable<any> {
    const params = new HttpParams().set('email', request);
    return this.httpClient.get<AttackPeriod[]>(
      this.baseDectionUrl+'attackPeriods',
      { params }
    );
  }

  public biggestThreath(request: string) : Observable<any> {
    const params = new HttpParams().set('email', request);
    return this.httpClient.get<AttackPeriodV2[]>(
      this.baseDectionUrl+'biggestThreath',
      { params }
    );
  }

  public addFeedback(feedBackModelDto: FeedBackModel): Observable<any> {
    return this.httpClient.put(
      this.baseDectionUrl + 'addFeedback',
      feedBackModelDto
    );
  }

  public getFeedback(message: string): Observable<any> {
    return this.httpClient.post(
      `${this.baseDectionUrl}getFeedback`,
      `"${message}"`,
      { headers: { 'Content-Type': 'application/json' } }
    );
  }
}
