export class ModelOutput {
    Category : number = 0;
    PredictedLabel: string = '';
    Score: number[] = [];
}