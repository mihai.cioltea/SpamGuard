export class PredictionRequestModel
{
    Sender : string = '';
    To: string = '';
    MailBody: string = '';
    Date: string = '';
}