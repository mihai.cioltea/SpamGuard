export class AttackModel {
    From : string = '';
    To: string = '';
    UserEmailProvider : string = '';
    Date: string = '';
    IsSpam: boolean = false;
}