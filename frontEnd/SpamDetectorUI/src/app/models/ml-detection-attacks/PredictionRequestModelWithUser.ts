import { PredictionRequestModel } from "./PredictionRequestModel";

export class PredictionRequestModelWithUser {
    PredictionModel: PredictionRequestModel = new PredictionRequestModel();
    UserEmail: string = '';
}