export class TableViewModel {
    index: number = 0;
    sender : string = '';
    to: string = '';
    subject: string = '';
    body: string = '';
    sendingDate: string = '';
    displayBody: string = '';
    displayDate: string = '';
    isMalicious: boolean = false;
    isHarmfull: boolean = false;
    isSafe: boolean = false;
    isChecked: boolean = false;
}