export class ActionToken {
    Id: number = 0;
    NoOfChecks: number = 0;
    Availability: Date = new Date();
    HasRevengeAction: boolean = false;
    UserEmail: string = '';
}