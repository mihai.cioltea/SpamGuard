export interface ISession {
    SessionId: string;
    PublicKey: string;
}