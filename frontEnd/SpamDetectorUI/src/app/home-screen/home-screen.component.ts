import { CommonModule } from '@angular/common';
import { Component, Injectable, OnInit, Type } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router, RouterModule, RouterOutlet } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { UserLogin } from '../models/user-management/userLogin';
import { ResetPasswordComponent } from '../reset-password/reset-password.component';
import { AuthService } from '../services/auth-service/auth.service';
import { SigninComponent } from '../signin/signin.component';
import { SignupComponent } from '../signup/signup.component';
import { ModalSize } from '../sizeConfig';
import { CookieService } from 'ngx-cookie-service';
import { HttpErrorResponse } from '@angular/common/http';
import { StripeFactoryService, StripeInstance } from 'ngx-stripe';
import { PaymentService } from '../services/payment-service/payment.service';
import { ISession } from '../models/payment/ISession';

@Injectable({
  providedIn: 'root',
})

@Component({
  selector: 'app-home-screen',
  standalone: true,
  imports: [RouterOutlet, CommonModule, FormsModule, MatButtonModule, MatDialogModule, ToastrModule, RouterModule],
  templateUrl: './home-screen.component.html',
  styleUrl: '../app.component.css'
})

export class HomeScreenComponent implements OnInit {
  public userLogin = new UserLogin();
  public loggedIn: boolean = false;
  public userEmail: string = '';
  public userName: string = '';
  public stripe!: StripeInstance;
  public emailFromTextField: string = '';

  constructor(private authService: AuthService,
    private modal: MatDialog,
    private size: ModalSize,
    private toastr: ToastrService,
    private router: Router,
    private cookieService: CookieService,
    private stripeFactory: StripeFactoryService,
    private paymentService: PaymentService,) {}

  async ngOnInit(): Promise<void> {
    const isServerRunning = await this.authService.checkServerStatus();
    if(!isServerRunning) {
      this.toastr.error('Server is not running');
    }

    if(sessionStorage.getItem('authToken')){
      this.loggedIn = true;
    }
    
    this.userEmail = sessionStorage.getItem('userEmail')!;
    this.userName = sessionStorage.getItem('userName')!;
    this.stripe = this.stripeFactory.create(this.paymentService.PublicStripeKey);
  }

//#region Logging and Authentication
  public openModal(componentType: Type<any>, configurationSmall?: MatDialogConfig<any>, configurationLarge?: MatDialogConfig<any>) {
    if (window.innerWidth >= 1300) {
      this.modal.open(componentType, configurationLarge);
    } else if (window.innerWidth < 1300) {
      this.modal.open(componentType, configurationSmall);
    }
  }
    
  public openSignUpModal() {
    const configSmall: MatDialogConfig<any> = {
      ...this.size.configSmallSU,
      data: { email: this.emailFromTextField }
    };
    const configLarge: MatDialogConfig<any> = {
      ...this.size.configLargeSU,
      data: { email: this.emailFromTextField }
    };

    this.openModal(SignupComponent, configSmall, configLarge);
    this.modal.afterAllClosed.subscribe(() => {
      this.emailFromTextField = '';
    });
  }

  public openSignInModal() {
    this.openModal(SigninComponent, this.size.configSmallSI, this.size.configLargeSI);
  }

  public openPasswordResetModal() {
    this.openModal(ResetPasswordComponent, this.size.configSmallSI, this.size.configLargeSI);
  }

  public routeToHome() {
    const authToken = sessionStorage.getItem('authToken');
    const refreshToken = this.cookieService.get('refreshToken');

    if(authToken && refreshToken) {
      this.router.navigate(['/main-page']);
    }
  }

  public logout() {
    this.authService.removeRefreshToken(this.userEmail).subscribe({
      next: () => {
        sessionStorage.clear();
        sessionStorage.clear();
        this.cookieService.delete('refreshToken');
        this.loggedIn = false;
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error.error, 'Log out Failed!');
      }
    });
  }
//#endregion
//#region Buy Tokens
  public goToCheckout(productPriceId: string) {
    if(productPriceId === 'CHECKTOKENPRICEID') {
      productPriceId = this.paymentService.CHECKTOKENPRICEID;
    }
    else {
      productPriceId = this.paymentService.REVENGETOKENPRICEID;
    }
    this.paymentService.requestMemberSession(productPriceId).subscribe({
      next: (response: ISession) => {
        sessionStorage.setItem('sessionId', response.SessionId);
        this.stripe.redirectToCheckout({ sessionId: response.SessionId }).subscribe();
      },
      error: (error : HttpErrorResponse) => {
        this.toastr.error(error.error, 'An error occurred while redirecting to checkout.'); 
      }
    });
  }
//#endregion
}
