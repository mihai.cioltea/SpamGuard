import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { HTTP_INTERCEPTORS, provideHttpClient } from '@angular/common/http';
import { routes } from './app.routes';
import { AuthInterceptor } from './services/auth-service/auth.interceptor';
import { provideToastr } from 'ngx-toastr';
import { provideAnimations } from '@angular/platform-browser/animations';
import { AuthGuard } from './services/auth-service/auth-guard.service';
import { CookieService } from 'ngx-cookie-service';
import { OAuthService, UrlHelperService, provideOAuthClient } from 'angular-oauth2-oidc';
import { GmailapiserviceService } from './services/gmail-api-service/gmailapiservice.service';
import { MLDetectionServiceService } from './services/ml-detection-service/mldetection-service.service';
import { AttackPeriodsComponent } from './attack-periods/attack-periods.component';
import { PaymentService } from './services/payment-service/payment.service';
import { StripeService } from 'ngx-stripe';
import { provideNgxStripe } from 'ngx-stripe';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    provideNgxStripe(),
    AuthGuard,
    CookieService,
    OAuthService,
    GmailapiserviceService,
    PaymentService,
    StripeService,
    UrlHelperService,
    MLDetectionServiceService,
    AttackPeriodsComponent,
    provideOAuthClient(),
    provideAnimations(),
    provideToastr()]
};

