import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { RouterModule } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { OpenAIService } from '../services/open-ai-service/open-ai.service';
import { MLDetectionServiceService } from '../services/ml-detection-service/mldetection-service.service';
import { AttackPeriodV2 } from '../models/ml-detection-attacks/attackPeriodV2';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { RevengeModel } from '../models/open-ai-revenge/revengeModel';
import { Observable, forkJoin } from 'rxjs';
 

@Component({
  selector: 'app-revenge-modal',
  standalone: true,
  imports: [MatDialogActions, MatButtonModule,
    MatDialogModule, ReactiveFormsModule,
    ToastrModule, RouterModule,
    MatProgressBarModule, CommonModule,
    MatListModule, MatRadioModule, 
    FormsModule],
  templateUrl: './revenge-modal.component.html',
  styleUrl: './revenge-modal.component.css'
})

export class RevengeModalComponent implements OnInit {
  public revengeForm: FormGroup<any> = new FormGroup<any>({});
  public showLoadingBar: boolean = false;
  public showLoadingBarOnGenerate: boolean = false;
  public showLoadingBarOnLoadAttackers: boolean = false;
  public showLoadingBarOnSend: boolean = false;
  public generatedText: string = '';
  public attackers: string[] = [];
  public emailChecked: string = '';
  private countGenerteClicks: number = 0;
  public selectedAttacker = new FormControl();
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private modalRef: MatDialogRef<RevengeModalComponent>,
    private toastr: ToastrService,
    private mlDetectionService: MLDetectionServiceService,
    private openAIService: OpenAIService) {}
  
  ngOnInit(): void {
    this.emailChecked = this.data.checkedEmail;
    this.getAttackes();
    this.countGenerteClicks = 3;
  }

  private getAttackes() {
    this.showLoadingBarOnLoadAttackers = true;
    this.mlDetectionService.biggestThreath(this.emailChecked).subscribe({
      next: (response: AttackPeriodV2[]) => {
        this.attackers = [];
        response.forEach(period => {
          this.attackers.push(period.Item2);
        });
        this.attackers = Array.from(new Set(this.attackers));
        this.selectedAttacker.setValue(this.attackers[0]);
        this.showLoadingBarOnLoadAttackers = false;
      },
      error: (error: HttpErrorResponse) => {
        this.showLoadingBarOnLoadAttackers = false;
        this.toastr.error(error.error,'Error occured at retriving attackers.');
      }
    })
  }

  public generateSpamText() {
    this.showLoadingBarOnGenerate = true;
    if(this.countGenerteClicks > 0) {
       this.openAIService.generateConversation().subscribe({
         next: (response: any) => {
            this.generatedText = response;
            this.showLoadingBarOnGenerate = false;
            this.countGenerteClicks--;
            this.toastr.success('You have successfully generated a message.');
         },
          error: (error: HttpErrorResponse) => {
            this.showLoadingBarOnGenerate = false;
            this.toastr.error(error.error,'Error occured at generating spam text.');
        }
      });
    }
    else {
      this.showLoadingBarOnGenerate = false;
      this.toastr.error('You have reached the maximum number of generations.');
    }
  }

  public sendAttack() {
    this.showLoadingBarOnSend = true;
    this.clearEmail();

    if(this.generatedText !== '' && this.selectedAttacker.value !== '') {
      const revengeModel = new RevengeModel();

      revengeModel.AttackerEmail = this.selectedAttacker.value;
      revengeModel.GeneratedEmail = this.generatedText!;
      revengeModel.UserEmail = this.emailChecked;

      const requests: Observable<any>[] = [];

      for (let i = 0; i < 10; i++) {
        requests.push(this.openAIService.sendAttack(revengeModel));
      }

      forkJoin(requests).subscribe({
        next: () => {
          this.toastr.success('You have successfully sent all attacks!');
          this.showLoadingBarOnSend = false;
          sessionStorage.setItem('sent','true');
          this.modalRef.close();
        },
        error: (error: HttpErrorResponse) => {
          this.showLoadingBarOnSend = false;
          this.toastr.error(error.error,'Error occurred while sending attacks.');
        }
      });
    }
    else {
      this.showLoadingBarOnSend = false;
      this.toastr.error('You have to generate a message before sending!');
    }
  }

  private clearEmail() {
    const pattern = /\<(.*@.*?)\>/;
    const match = pattern.exec(this.selectedAttacker.value);

    if (match && match[1]) {
        this.selectedAttacker.setValue(match[1]);
    }
  }
}
