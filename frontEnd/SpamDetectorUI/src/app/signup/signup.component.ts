import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogRef } from '@angular/material/dialog';
import { MatDialogModule} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { AuthService } from '../services/auth-service/auth.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserRegister } from '../models/user-management/userRegister';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { HomeScreenComponent } from '../home-screen/home-screen.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-signup',
  standalone: true,
  imports: [MatDialogActions, MatButtonModule,
      MatDialogModule, ReactiveFormsModule,
      ToastrModule, MatProgressBarModule,
      CommonModule],
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.css'
})

export class SignupComponent {
  public signUpForm: FormGroup<any> = new FormGroup<any>({});
  public showLoadingBar: boolean = false;
  public emailFromHomeScreen: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private homeScreenComponent: HomeScreenComponent,
    private modalRef: MatDialogRef<SignupComponent>,
    private fb: FormBuilder,
    private authService: AuthService,
    private toastr: ToastrService
    ) {}

  ngOnInit(): void {
    this.emailFromHomeScreen = this.data.email;
    this.signUpForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      email: [this.emailFromHomeScreen, [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  public register(){
    this.showLoadingBar = true;
    if (this.signUpForm) {
      const user: UserRegister = new UserRegister();

      user.firstName = this.signUpForm.get('firstName')?.value;
      user.lastName = this.signUpForm.get('lastName')?.value;
      user.email = this.signUpForm.get('email')?.value;
      user.password = this.signUpForm.get('password')?.value;
      user.role = 'User';

      if(user.email && user.password && user.firstName && user.lastName) {
        this.authService.register(user).subscribe({
          next: () => {
            this.showLoadingBar = false;
            this.toastr.success('Registration Success!');
            this.modalRef.close();
          },
          error: (error : HttpErrorResponse) => {
            this.showLoadingBar = false;
            this.toastr.error(error.error,'Registration Failed!');
          }
        }); 
      }
      else {
        this.showLoadingBar = false;
        this.toastr.error('All fileds are required!');
      }
    }
  }
  
  public openSignin() {
    this.homeScreenComponent.openSignInModal();
    this.modalRef.close();
  }
}
