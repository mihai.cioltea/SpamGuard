import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogActions, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { UserLogin } from '../models/user-management/userLogin';
import { AuthService } from '../services/auth-service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { HomeScreenComponent } from '../home-screen/home-screen.component';
import { UserResponse } from '../models/user-management/userResponse';
import { CookieService } from 'ngx-cookie-service';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-signin',
  standalone: true,
  imports: [MatDialogActions, MatButtonModule,
     MatDialogModule, ReactiveFormsModule,
     ToastrModule, RouterModule,
     MatProgressBarModule, CommonModule],
  templateUrl: './signin.component.html',
  styleUrl: './signin.component.css'
})

export class SigninComponent implements OnInit {
  public signInForm: FormGroup<any> = new FormGroup<any>({});
  public showLoadingBar: boolean = false;

  constructor(private homeScreenComponent: HomeScreenComponent,
    private modalRef: MatDialogRef<SigninComponent>,
    private authService: AuthService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private cookieService: CookieService) {}

  ngOnInit(): void {
    this.signInForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  public openSignup() {
    this.homeScreenComponent.openSignUpModal();
    this.modalRef.close();
  }

  public openForgotPassword() {
    this.showLoadingBar = true;
    if (this.signInForm) {
      const email = this.signInForm.get('email')?.value;
      
      if (email) {
        this.authService.forgotPassword(email).subscribe({
          next: () => {
            this.showLoadingBar = false;
            this.toastr.success('You have been sent an email with instructions!');
            this.homeScreenComponent.openPasswordResetModal();
            this.modalRef.close();
          },
          error: (error: HttpErrorResponse) => {
            this.showLoadingBar = false;
            this.toastr.error(error.error);
          }
        });
      }
      else {
        this.showLoadingBar = false;
        this.toastr.error('Email is required!');
      }
    }
  }

  public login() {
    this.showLoadingBar = true;
    if (this.signInForm) {
      const user: UserLogin = new UserLogin();

      user.email = this.signInForm.get('email')?.value;
      user.password = this.signInForm.get('password')?.value;

      if(user.password && user.email) {
        this.authService.login(user).subscribe({
          next: (userResponse: UserResponse) => {
            this.showLoadingBar = false;
            sessionStorage.setItem('authToken', userResponse.AuthTOKEN);
            this.cookieService.set('refreshToken', userResponse.RefreshTOKEN);
            this.toastr.success('Log in Success!');
            const userEmail = user.email;
            sessionStorage.setItem('userEmail', userEmail);
            this.getUserName(userEmail);
            this.modalRef.close();
            this.router.navigate(['/main-page'], {state: {userEmail}});
          },
          error: (error : HttpErrorResponse) => {
            this.showLoadingBar = false;
            this.toastr.error(error.error,'Log in Failed!');
          }
        }); 
      }
      else {
        const email = user.email === '';
        const password = user.password === '';
        if(email && password) {
          this.showLoadingBar = false;
          this.toastr.error('Email and password are required!');
        }
        else {
          if(email) {
            this.showLoadingBar = false;
            this.toastr.error('Email is required!');
          }
          else {
            this.showLoadingBar = false;
            this.toastr.error('Password is required!');
          }
        }
      }
    }
  }
  
  private getUserName(email: string) {
    this.authService.getUserInfo(email).subscribe({
      next: (username: string) => {
        sessionStorage.setItem('userName', username);
      },
      error: (error : HttpErrorResponse) => {
        this.toastr.error(error.error,'Failed to fetch user information!');
      }
    });
  }
}
