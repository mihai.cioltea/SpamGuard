import { Component, Input, OnInit } from '@angular/core';
import { Chart } from 'chart.js/auto';
import { MLDetectionServiceService } from '../services/ml-detection-service/mldetection-service.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AttackPeriod } from '../models/ml-detection-attacks/attackPeriod';

@Component({
  selector: 'app-attack-periods',
  standalone: true,
  imports: [],
  templateUrl: './attack-periods.component.html',
  styleUrl: './attack-periods.component.css'
})

export class AttackPeriodsComponent implements OnInit {
  public chart: Chart | null= null;
  public hamEmails: number[] = [];
  public spamEmails: number[] = [];
  public datesForChecks: string[] = [];
  @Input()
  public email: string = '';

  constructor(private mlDetectionService: MLDetectionServiceService,
    private toastr: ToastrService) {}
    
  async ngOnInit(): Promise<void> {  
    await this.populateData(this.email);
  }

  private createChart() : Chart {
    return new Chart("attack-periods", {
      type: 'bar', 
      data: {
        labels: this.datesForChecks, 
        datasets: [
        {
          label: "Clean Emails",
          data: this.hamEmails,
          backgroundColor: 'rgb(63, 185, 80)'
        },
        {
          label: "Spam Emails",
          data: this.spamEmails,
          backgroundColor: '#7C72FF'
        }]
      },  
      options: {
        aspectRatio:2.5
      }  
    });
  }

  public populateData(email: string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.mlDetectionService.attackPeriods(email).subscribe({
        next: (response: AttackPeriod[]) => {
          this.datesForChecks = [];
          this.hamEmails = [];
          this.spamEmails = [];
          response.forEach(period => {
            this.datesForChecks.push(period.Item1);
            this.hamEmails.push(period.Item2);
            this.spamEmails.push(period.Item3);
          });
          this.chart = this.createChart();
          resolve();
        },
        error: (error: HttpErrorResponse) => {
          this.toastr.error(error.error, 'Error while loading attack periods!');
          reject(error);
        }
      });
    });
  }
}
