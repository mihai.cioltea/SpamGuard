import { Routes } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { HomeScreenComponent } from './home-screen/home-screen.component';
import { AuthGuard } from './services/auth-service/auth-guard.service';

export const routes: Routes = [
    { 
        path: '', 
        component: HomeScreenComponent,
        pathMatch: 'full'
    },
    { 
        path: 'main-page',
        component: MainPageComponent,
        canActivate: [AuthGuard]
    }
];
