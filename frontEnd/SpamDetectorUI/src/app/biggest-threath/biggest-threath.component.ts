import { Component, Input, OnInit } from '@angular/core';
import { AttackPeriodV2 } from '../models/ml-detection-attacks/attackPeriodV2';
import { MLDetectionServiceService } from '../services/ml-detection-service/mldetection-service.service';
import { ToastrService } from 'ngx-toastr';
import { Chart } from 'chart.js';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-biggest-threath',
  standalone: true,
  imports: [],
  templateUrl: './biggest-threath.component.html',
  styleUrl: './biggest-threath.component.css'
})

export class BiggestThreathComponent implements OnInit {
  public chart: Chart | null= null;
  public totalScore: number[] = [];
  public attackers: string[] = [];
  public datesForChecks: string[] = [];
  @Input()
  public email: string = '';

  constructor(private mlDetectionService: MLDetectionServiceService,
    private toastr: ToastrService) {}
    
  async ngOnInit(): Promise<void> {  
    await this.populateData(this.email);
  }

  private createChart() : Chart {
    const chartOptions: any = {
      aspectRatio: 2.5,
      plugins: {
        tooltip: {
          callbacks: {
            label: (context: any) => {
              const attacker = this.attackers[context.dataIndex];
              return `Attacker: ${attacker} with: ${context.parsed.y} mails.`;
            }
          }
        }
      }
    };
  
    return new Chart("biggest-threath", {
      type: 'bar',
      data: {
        labels: this.datesForChecks,
        datasets: [{
          label: 'Total Number of Spam Emails',
          data: this.totalScore,
          backgroundColor: '#7C72FF',
        }]
      },
      options: chartOptions
    });
  }

  public populateData(email: string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.mlDetectionService.biggestThreath(email).subscribe({
        next: (response: AttackPeriodV2[]) => {
          this.datesForChecks = [];
          this.attackers = [];
          this.totalScore = [];
          response.forEach(period => {
            this.datesForChecks.push(period.Item1);
            this.attackers.push(period.Item2);
            this.totalScore.push(period.Item3);
          });
          this.chart = this.createChart();
          resolve();
        },
        error: (error: HttpErrorResponse) => {
          this.toastr.error(error.error, 'Error while loading biggest threath!');
          reject(error);
        }
      });
    });
  }
}
