import { Component, ComponentFactoryResolver, OnInit, Renderer2, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthService } from '../services/auth-service/auth.service';
import { Router, RouterModule } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpErrorResponse,} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { GmailapiserviceService } from '../services/gmail-api-service/gmailapiservice.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { interval, lastValueFrom } from 'rxjs';
import { CommonModule } from '@angular/common';
import { TableViewModel } from '../models/user-management/tabelviewmodel';
import { Buffer } from 'buffer';
import { ClipboardService } from 'ngx-clipboard';
import { Sort, MatSortModule } from '@angular/material/sort';
import { TooltipPosition, MatTooltipModule } from '@angular/material/tooltip';
import { MLDetectionServiceService } from '../services/ml-detection-service/mldetection-service.service';
import { ModelOutput } from '../models/ml-detection-attacks/modelOutput';
import { AttackPeriodsComponent } from "../attack-periods/attack-periods.component";
import { BiggestThreathComponent } from '../biggest-threath/biggest-threath.component';
import { SaveAttackRequestModel } from '../models/ml-detection-attacks/SaveAttackRequestModel';
import { PredictionRequestModelWithUser } from '../models/ml-detection-attacks/PredictionRequestModelWithUser';
import { ActionToken } from '../models/user-management/actionToken';
import { PaymentService } from '../services/payment-service/payment.service';
import { StripeFactoryService, StripeInstance } from 'ngx-stripe';
import { ISession } from '../models/payment/ISession';
import { IGoogleUser } from '../models/user-management/IGoogleUser';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { OpenAIService } from '../services/open-ai-service/open-ai.service';
import { MatDialog } from '@angular/material/dialog';
import { RevengeModalComponent } from '../revenge-modal/revenge-modal.component';
import { FeedBackModel } from '../models/ml-detection-attacks/feedbackModel';

@Component({
    selector: 'app-main-page',
    standalone: true,
    templateUrl: './main-page.component.html',
    styleUrl: '../app.component.css',
    imports: [RouterModule, ToastrModule,
      ReactiveFormsModule, OAuthModule,
      CommonModule, MatSortModule,
      MatTooltipModule, AttackPeriodsComponent,
      BiggestThreathComponent, MatProgressBarModule]
})

export class MainPageComponent implements OnInit {
  public userEmail: string = '';
  public userInfo?: IGoogleUser;
  public isValid : boolean = false;
  public showButtons: boolean = false;
  public mails: TableViewModel[] = [];
  public sortedMails: TableViewModel[] = [];
  public noOfEmails: number = 10;
  public position: TooltipPosition = "below";
  public userName: string = '';
  public showAttackPeriodsComponent: boolean = false;
  public showBiggestThreathComponent: boolean = false;
  public checkedEmail: string = '';
  public checkTokens: number = 0;
  private componentRef: any;   
  @ViewChild('chartContainer', { read: ViewContainerRef }) 
  private chartContainer!: ViewContainerRef;
  private info: string = '1.Provide your agreement to use your inbox.<br>'+
  '2.Select the number of mails to load.<br>'+
  '3.Hit the display button.';
  private stripe!: StripeInstance;
  public showLoadingBarOnTable: boolean = false;
  public showLoadingBarOnBuy: boolean = false;
  public showLoadingBarOnGraphs: boolean = false;
  private showAnalyticsCounter: number = 0;
  private lastClickedButtonId: string = '';
  public revengeToken: string = '';

  constructor(private authService: AuthService,
    private router: Router,
    private cookieService: CookieService,
    private toastr: ToastrService,
    private gmailApiService: GmailapiserviceService,
    private clipboardService: ClipboardService,
    private mlDetectionService: MLDetectionServiceService,
    private resolver: ComponentFactoryResolver,
    private renderer: Renderer2,
    private paymentService: PaymentService,
    private stripeFactory: StripeFactoryService,
    private openAIService: OpenAIService,
    private modal: MatDialog) {
     }

  async ngOnInit(): Promise<void> {
    const isServerRunning = await this.authService.checkServerStatus();
    if(!isServerRunning) {
      this.toastr.error('Server is not running');
    }

    this.userEmail= sessionStorage.getItem('userEmail')!;
    this.userName = sessionStorage.getItem('userName')!;
    this.isValid = false;

    this.checkForAccessDeniedError();
    this.checkForAccessPermited();

    this.stripe = this.stripeFactory.create(this.paymentService.PublicStripeKey);
    this.restockTokens();
    this.getActionToken(); 
  }

  private checkForAccessDeniedError(): void {
    const urlParams = new URLSearchParams(window.location.hash.replace('#', '?'));
    if (urlParams.has('error') && urlParams.get('error') === 'access_denied') {
      this.router.navigate(['/main-page']);
    }
  }

  private checkForAccessPermited(): void {
    const urlParams = new URLSearchParams(window.location.hash.replace('#', '?'));
    if (urlParams.has('access_token') || sessionStorage.getItem('access_token')) {
      this.agreeProvider();
    }
  }
//#region Navigation Bar
  public logout() {
    this.authService.removeRefreshToken(this.userEmail).subscribe({
      next: () => {
        sessionStorage.clear();
        sessionStorage.clear();
        this.cookieService.delete('refreshToken');
        this.router.navigate(['']);
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error.error, 'Log out Failed!');
      }
    });
  }

  public routeToHome() {
    const authToken = sessionStorage.getItem('authToken');
    const refreshToken = this.cookieService.get('refreshToken');
    const currentPage = this.router.url === '/main-page';

    if(authToken && refreshToken && currentPage) {
      this.router.navigate(['/main-page']);
    }
  }
//#endregion
//#region Inbox Security Check
  public checkGmailAccount() {
    this.gmailApiService.logIntoMailProvider();
  }

  public logOutFromGoogleAccount(){
    sessionStorage.removeItem('provider');
    sessionStorage.removeItem('userProfile');
    this.enableAgreeButton();
    this.gmailApiService.signOut();
    this.gmailApiService.logIntoMailProvider();
  }

  public getActionToken() {
    this.authService.getActionToken(this.userEmail).subscribe({
      next: (actionToken : ActionToken) => {
        this.checkTokens = actionToken.NoOfChecks;
        actionToken.HasRevengeAction === true? this.revengeToken = 'Active': '';
      },
      error: (error: HttpErrorResponse) => {
        this.toastr.error(error.error, 'Get Action Token Failed!');
      }
    });
  }

  private decodeBase64(data: string): string {
    return Buffer.from(data, 'base64').toString('utf-8');
  }

  public sortData(sort: Sort) {
    const data = this.mails.slice();

    if (!sort.active || sort.direction === '') {
      this.sortedMails = data;
      return;
    }

    this.sortedMails = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'index':
          return this.compare(a.index, b.index, isAsc);
        case 'sender':
          return this.compare(a.sender, b.sender, isAsc);
        case 'subject':
          return this.compare(a.subject, b.subject, isAsc);
        case 'date':
          return this.compare(a.sendingDate, b.sendingDate, isAsc);
        default:
          return 0;
      }
    });
  }

  private compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  private formatDate(dateString: string, noFormat: number) : string {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const hours = date.getUTCHours().toString().padStart(2, '0');
    const minutes = date.getUTCMinutes().toString().padStart(2, '0');
    const seconds = date.getUTCSeconds().toString().padStart(2, '0');
    const milliseconds = date.getUTCMilliseconds().toString().padStart(3, '0');
    let formattedDate ='';

    if(noFormat === 1) {
      formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
    }
    else {
      formattedDate = `${day}/${month}/${year} ${hours}:${minutes}`;
    }

    return formattedDate;
  }

  public async getEmails() {
    this.showLoadingBarOnTable = true;
    if(sessionStorage.getItem('userProfile') == null) {
      this.toastr.error('Please provide your aggreement to use your inbox!', 'Aggreement Failed!');
      return;
    }

    this.mails = [];
    const userId = sessionStorage.getItem('userProfile') as string
    const messages = await lastValueFrom(this.gmailApiService.emails(userId, this.noOfEmails));
    let index = 1;

    messages.messages.forEach( (element: any) => {
      this.gmailApiService.getMail(userId, element.id).subscribe({
        next : (email : any) => { 
          this.showLoadingBarOnTable = false;  
          const sender = email.payload.headers.find((header : {name: string; value: string}) => header.name === 'From')?.value;
          const subject = email.payload.headers.find((header : {name: string; value: string}) => header.name === 'Subject')?.value;
          const sendingDate = email.payload.headers.find((header : {name: string; value: string}) => header.name === 'Date')?.value;
          const to = email.payload.headers.find((header : {name: string; value: string}) => header.name === 'Delivered-To')?.value;
          let body ='';

          if(email.payload.body.size >0) {
            body+=email.payload.body.data;
          }
          else {
            email.payload.parts.forEach((part : {body: {data: string}, mimeType: string}) => {
              if(part.mimeType === 'text/plain') {
                body += part.body.data || '';
              }
            });
          }

          body = this.decodeBase64(body);
          const regex = /^(?:[\s\S]*?)(?=[^\s])/;
          const match = regex.exec(body);
          let bodyFromFirstLetter ='';

          if (match && match.index !== undefined) {
            const startIndex = match.index;
            bodyFromFirstLetter = body.substring(startIndex);
            bodyFromFirstLetter.trim();
          }

          const tableViewModel = new TableViewModel();
          tableViewModel.index = index++;
          tableViewModel.sender = sender;
          tableViewModel.subject = subject;
          tableViewModel.to = to;
          tableViewModel.displayDate = this.formatDate(sendingDate, 0);
          tableViewModel.sendingDate = this.formatDate(sendingDate, 1); 
          
          tableViewModel.displayBody = email.snippet + '...';
          tableViewModel.body = body === ''? email.snippet : body;

          this.mails.push(tableViewModel); 
      }});
    });

    this.sortedMails = this.mails;
  }

  public checkEmailVulerability(mail: TableViewModel) {
    const predictionRequestModel = new PredictionRequestModelWithUser();
    const saveAttackModel = new SaveAttackRequestModel();
    const regex = /^([^@]+)@gmail\.com$/;

    predictionRequestModel.UserEmail =saveAttackModel.UserEmail = this.userEmail;
    predictionRequestModel.PredictionModel.Sender = saveAttackModel.AttackModel.From = mail.sender;
    predictionRequestModel.PredictionModel.To = saveAttackModel.AttackModel.To = mail.to;
    predictionRequestModel.PredictionModel.Date = saveAttackModel.AttackModel.Date = mail.sendingDate;
    
    if(regex.test(mail.to)) {
      saveAttackModel.AttackModel.UserEmailProvider = 'Google Mail';
    }

    if(mail.body.length > 20000) {
      predictionRequestModel.PredictionModel.MailBody = mail.displayBody;
    }
    else {
      predictionRequestModel.PredictionModel.MailBody = mail.body;
    }

    let hamScoreNumber = 0;
    let spamScoreNumber = 0;
    let hamScore;
    let spamScore;

    this.mlDetectionService.messagePrediction(predictionRequestModel).subscribe({
      next: (response: ModelOutput) => {
        hamScore = (response.Score[0]*100).toFixed(2);
        spamScore = (response.Score[1]*100).toFixed(2);
        hamScoreNumber = response.Score[0];
        spamScoreNumber = response.Score[1];

        if(response.PredictedLabel === 'ham') {
          if(response.Score[0]> 0.50 && response.Score[0]<0.80) {
            this.toastr.warning('The email is considered harmfull with a score of: ' + hamScore + '%!');
            mail.isHarmfull = true;
          }
          else {
            this.toastr.success('The email is considered safe with a score of: ' + hamScore + '%!');
            mail.isSafe = true;
          }
        }
        else {
          if(response.Score[1]> 0.50 && response.Score[1]<0.80) {
            this.toastr.warning('The email is considered harmfull with a score of: ' + spamScore + '%!');
            mail.isHarmfull = true;
          }
          else {
            this.toastr.error('The email is considered malicious with a score of: ' + spamScore + '%!');
            mail.isMalicious = true;
          }
        }

        mail.isChecked = true;

        if(spamScoreNumber > hamScoreNumber) {
          saveAttackModel.AttackModel.IsSpam = true;
        }
        else {
          saveAttackModel.AttackModel.IsSpam = false;
        }
        this.saveAttack(saveAttackModel);
      },
      error: (error : HttpErrorResponse) => {
        if(error.error === 'You have reached the maximum amount of checks for today!') {
          this.getActionToken();
          this.toastr.error('You have reached the maximum amount of checks for today!');
        }
        else {
          this.toastr.error(error.error,'Check has failed!');
        }
      }
    }); 
  }

  private saveAttack(saveAttackRequestModel: SaveAttackRequestModel) {
    this.mlDetectionService.saveAttack(saveAttackRequestModel).subscribe({
      next: () => {
        this.toastr.success('The potential attack has been stored succesfully!');
        this.getActionToken();
      },
      error:  (error : HttpErrorResponse) => {
        if(error.error !== 'You have reached the maximum amount of checks for today!') {
          this.toastr.error(error.error,'Saving the spam email has failed!');
        }
      }
    });
  }

  public copyEmailBody(body: string) {
    this.clipboardService.copy(body);
    this.toastr.success('Email body copied to clipboard!');
  }

  public chooseNoOfEmails(nuumberOfEmails: number) {
    switch (nuumberOfEmails) {
      case 10:
        this.noOfEmails = 10;
        this.mails = [];
        break;
      case 20:
        this.noOfEmails = 20;
        this.mails = [];
        break;
      case 50:
        this.noOfEmails = 50;
        this.mails = [];
        break;
      case 100:
        this.noOfEmails = 100;
        this.mails = [];
        break;
      case 200:
        this.noOfEmails = 200;
        this.mails = [];
        break;
      case 400:
        this.noOfEmails = 400;
        this.mails = [];
        break;
      default:
        this.noOfEmails = 10;
        this.mails = [];
    }
  }

  public agreeProvider() {
    const message = 'We are currently using your Google Mail provider!';
    sessionStorage.setItem('provider', 'Google Mail');
    this.checkGmailAccount();
    this.toastr.success(message, 'Aggreement completed!');
    this.disableAgreeButton();
    this.showButtons = true;
  }

  private disableAgreeButton() {
    const agreeButton = document.getElementById("agreeButton") as HTMLButtonElement;
    const provider = sessionStorage.getItem('provider') !== null;

    if(agreeButton && provider) {
      this.renderer.setAttribute(agreeButton, 'disabled', 'true');
      this.showButtons = true;
    }
  }

  private enableAgreeButton() {
    const agreeButton = document.getElementById("agreeButton") as HTMLButtonElement;
    const provider = sessionStorage.getItem('provider') === null;

    if(agreeButton && provider) {
      this.renderer.setAttribute(agreeButton, 'enabled', 'true');
      this.showButtons = false;
    }
  }

  public showInfo() {
    this.toastr.info(this.info, 'Quick information tour', {
      enableHtml: true,
      timeOut: 10000,
      positionClass: 'toast-top-center'
    });
  }

  public goToCheckout(productPriceId: string) {
    this.showLoadingBarOnBuy = true;
    if(productPriceId === 'CHECKTOKENPRICEID') {
      productPriceId = this.paymentService.CHECKTOKENPRICEID;
    }
    else {
      productPriceId = this.paymentService.REVENGETOKENPRICEID;
    }

    this.paymentService.requestMemberSession(productPriceId).subscribe({
      next: (response: ISession) => {
        this.showLoadingBarOnBuy = false;
        sessionStorage.setItem('sessionId', response.SessionId);
        sessionStorage.setItem('productId', productPriceId);
        this.stripe.redirectToCheckout({ sessionId: response.SessionId }).subscribe();
      },
      error: (error : HttpErrorResponse) => {
        this.showLoadingBarOnBuy = false;
        this.toastr.error(error.error, 'An error occurred while redirecting to checkout.'); 
      }
    });
  }

  private restockTokens() {
    const sessionId = sessionStorage.getItem('sessionId')!;
    const priceId = sessionStorage.getItem('productId')!;

    if(sessionId) {
      this.paymentService.sessionDetails(sessionId, this.userEmail, priceId).subscribe({
        next: (response: any) => {
          if(response === true) {
            this.toastr.success('The payment was successful', 'Payment');
            this.getActionToken();
          }
          else {
            this.toastr.error('The payment was denied', 'Payment');
          }
        },
        error: (error : HttpErrorResponse) => {
          this.toastr.error(error.error, 'An error occurred while restocking tokens.'); 
        },
        complete: () => {
          sessionStorage.removeItem('sessionId');
        }
      });
    }
  } 

  private addFeedback(message: string, value: string) {
    const feedback = new FeedBackModel();
    feedback.IsOK = value === 'Yes'? true : false;
    feedback.Message = message;
    this.mlDetectionService.addFeedback(feedback).subscribe({
      next: () => {
        this.toastr.success('Thank you for your feedback!', 'Feedback');
      },
      error: (error : HttpErrorResponse) => {
        this.toastr.error('An error occurred while adding feedback.'); 
      }
    });
  }

  public futureImprovement(message: string, value: string) {
    this.mlDetectionService.getFeedback(message).subscribe({
      next: (response: boolean) => {
        if(response === true) {
          this.toastr.success('This message is already in our feedback list\nThank you!', 'Feedback');
        }
        else {
          this.addFeedback(message, value);
        }
      },
      error: (error : HttpErrorResponse) => {
        this.toastr.error('An error occurred while adding feedback.'); 
      }
    });  
  }
//#endregion 
//#region Insights 
  public showAnalytics(email: string, graphNo: number) {
    if(this.checkEmail()) {
      return;
    }

    const showTopRisk = document.getElementById("showTopRisk") as HTMLButtonElement;
    const showAttackTrends = document.getElementById("showAttackTrends") as HTMLButtonElement;

    if (!this.lastClickedButtonId || this.lastClickedButtonId !== (graphNo === 1 ? "showTopRisk" : "showAttackTrends")) {
      showTopRisk.disabled = true;
      showAttackTrends.disabled = true;

      if (this.showAnalyticsCounter % 2 === 0) {
        this.startTimer(1); 
      }
    } 
    else {
      showTopRisk.disabled = false;
      showAttackTrends.disabled = false;
    }

    if (this.checkEmail()) {
      return;
    }

    if (graphNo === 1) {
      this.showBiggestThreathComponent = true;
      this.showAttackPeriodsComponent = false;
      this.createComponent(BiggestThreathComponent, email);
    } 
    else {
      this.showAttackPeriodsComponent = true;
      this.showBiggestThreathComponent = false;
      this.createComponent(AttackPeriodsComponent, email); 
    }

    this.lastClickedButtonId = graphNo === 1 ? "showTopRisk" : "showAttackTrends";
    this.showAnalyticsCounter++;
  }

  private startTimer(seconds: number) {
    this.showLoadingBarOnGraphs = true;

    const timer$ = interval(1000).subscribe((sec) => {
      if (sec === seconds) {
        this.showLoadingBarOnGraphs = false;
        
        if (this.lastClickedButtonId) {
          const clickedButton = document.getElementById(this.lastClickedButtonId) as HTMLButtonElement;
          clickedButton.disabled = false;
        }

        this.toastr.success('Click again to open it.','The loading of the chart is complete.', {
          timeOut: 5000
        });

        timer$.unsubscribe();
      }
    });
  }

  private createComponent(componentType: any, email: string) {
    try {
      this.destroyComponent();
      const factory = this.resolver.resolveComponentFactory(componentType);
      this.componentRef = this.chartContainer?.createComponent(factory);
      this.componentRef.instance.email = email;
    } 
    catch (error) {
      return;
    }
  }

  private destroyComponent() {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.chartContainer?.clear();
    }
  }

  private checkEmail() : boolean {
    const idTokenClaimsObjString = sessionStorage.getItem('id_token_claims_obj');
    const idTokenClaimsObj = idTokenClaimsObjString ? JSON.parse(idTokenClaimsObjString) : null;
    this.checkedEmail = idTokenClaimsObj === null ? '' : idTokenClaimsObj.email;
    
    if(sessionStorage.getItem('userProfile') == null || this.checkedEmail === '') {
      this.toastr.error('Please provide your aggreement to use your inbox!', 'Aggreement Failed!');
      return true;
    }
    return false;
  }

  public getRevenge() {
    if(this.checkEmail()) {
      return;
    }

    if(this.revengeToken !== '') {
      this.modal.open(RevengeModalComponent, {
        data: { checkedEmail: this.checkedEmail },
        width: '50%',
        disableClose: true
      });

      this.modal.afterAllClosed.subscribe(() => {
        const sent = sessionStorage.getItem('sent');
        if(sent === 'true') {
          this.removeRevengeToken(this.userEmail);
          location.reload();
          sessionStorage.removeItem('sent');
        }
      });
    }
    else {
      this.toastr.error('You have to buy a revenge token in order to use this feature', 'Revenge Token Failed!');
    }
  }

  private removeRevengeToken(userEmail: string) {
    this.openAIService.removeHasRevenge(userEmail).subscribe({
      next: () => {
        this.revengeToken = '';
        this.toastr.success('The Revenge Token has been removed', 'Revenge Token Removed');
      },
      error: (error : HttpErrorResponse) => {
        this.toastr.error(error.error, 'An error occurred while removing Revenge Token.'); 
      }
    })
  }
//#endregion 
}
