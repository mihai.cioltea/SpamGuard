import { Injectable } from "@angular/core";
import { MatDialogConfig } from "@angular/material/dialog";

@Injectable({
    providedIn: 'root',
})

export class ModalSize{
  configLargeSU: MatDialogConfig<any> = {
      width: '40%'
  };
  configSmallSU: MatDialogConfig<any> = {
      width: '60%'
  };
  configLargeSI: MatDialogConfig<any> = {
      width: '30%'
  }
  configSmallSI: MatDialogConfig<any> = {
      width: '50%'
  }
}