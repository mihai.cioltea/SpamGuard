﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using MediatR;
using SpamDetector.Controllers;
using SpamDetector.Features.UserManagement.Register.Commands.AddUser;
using SpamDetector.Features.UserManagement.Register.Dtos;
using SpamDetector.Models.UserManagement;
using SpamDetector.Features.UserManagement.Login.Queries.GetUser;
using SpamDetector.Features.UserManagement.Login.Commands.UpdateRefreshTokenByUser;
using SpamDetector.Features.UserManagement.ResetPassword.Commands.AddPasswordResetToken;
using SpamDetector.Features.UserManagement.ResetPassword.Commands.UpdatePassword;
using SpamDetector.Features.UserManagement.Logout.Commands.DeleteRefreshTokenByUser;
using SpamDetector.Features.UserManagement.Login.Queries.GetUserInfo;
using SpamDetector.Features.UserManagement.Login.Queries.GetActionToken;

namespace SpamDetector.Tests.UserManagement
{
    public class UserManagementControllerTests
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly UserManagementController _controller;

        public UserManagementControllerTests()
        {
            _mediatorMock = new Mock<IMediator>();
            _controller = new UserManagementController(_mediatorMock.Object);
        }

        [Fact]
        public void CheckServerStatus_ReturnsOkResult()
        {
            //Arrange + Act
            var result = _controller.CheckServerStatus() as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task Register_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userRegisterDto = new UserRegisterDto();
            var user = new User();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddUserCommand>(), default)).ReturnsAsync(user);

            // Act
            var result = await _controller.Register(userRegisterDto);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
            Assert.Equal(user, okResult.Value);
        }

        [Fact]
        public async Task Register_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userRegisterDto = new UserRegisterDto();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddUserCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.Register(userRegisterDto);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task Login_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userLogin = new UserLogin();
            var userResponse = new UserResponse();
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetUserQuery>(), default)).ReturnsAsync(userResponse);

            // Act
            var result = await _controller.Login(userLogin);

            // Assert
            var okResult = Assert.IsType<ActionResult<UserResponse>>(result);
            Assert.IsType<OkObjectResult>(okResult.Result);
            Assert.Equal(200, (okResult.Result as OkObjectResult)?.StatusCode);
            Assert.Equal(userResponse, (okResult.Result as OkObjectResult)?.Value);
        }

        [Fact]
        public async Task Login_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userLogin = new UserLogin();
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetUserQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.Login(userLogin);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result.Result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task RefreshToken_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userLogin = new UserLogin();
            _mediatorMock.Setup(m => m.Send(It.IsAny<UpdateRefreshTokenByUserCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.RefreshToken(userLogin);

            // Assert
            var okResult = Assert.IsType<OkResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task RefreshToken_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userLogin = new UserLogin();
            _mediatorMock.Setup(m => m.Send(It.IsAny<UpdateRefreshTokenByUserCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.RefreshToken(userLogin);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task ForgotPassword_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var email = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddPasswordResetTokenCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.ForgotPassword(email);

            // Assert
            var okResult = Assert.IsType<OkResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task ForgotPassword_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var email = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddPasswordResetTokenCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.ForgotPassword(email);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task ResetPassword_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userPasswordReset = new UserPasswordReset();
            _mediatorMock.Setup(m => m.Send(It.IsAny<UpdatePasswordCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.ResetPassword(userPasswordReset);

            // Assert
            var okResult = Assert.IsType<OkResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userPasswordReset = new UserPasswordReset();
            _mediatorMock.Setup(m => m.Send(It.IsAny<UpdatePasswordCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.ResetPassword(userPasswordReset);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task RemoveRefreshToken_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userEmail = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<DeleteRefreshTokenByUserCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.RemoveRefreshToken(userEmail);

            // Assert
            var okResult = Assert.IsType<OkResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task RemoveRefreshToken_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userEmail = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<DeleteRefreshTokenByUserCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.RemoveRefreshToken(userEmail);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task ProvideUserInfo_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userEmail = "test@example.com";
            var userInfo = "User info";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetUserInfoQuery>(), default)).ReturnsAsync(userInfo);

            // Act
            var result = await _controller.ProvideUserInfo(userEmail);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
            Assert.Equal(userInfo, okResult.Value);
        }

        [Fact]
        public async Task ProvideUserInfo_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userEmail = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetUserInfoQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.ProvideUserInfo(userEmail);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task ProvideActionToken_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userEmail = "test@example.com";
            var actionToken = new ActionToken();
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetActionTokenQuery>(), default)).ReturnsAsync(actionToken);

            // Act
            var result = await _controller.ProvideActionToken(userEmail);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
            Assert.Equal(actionToken, okResult.Value);
        }

        [Fact]
        public async Task ProvideActionToken_ExceptionThrown_ReturnsBadRequest()
        {
            // Arrange
            var userEmail = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetActionTokenQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.ProvideActionToken(userEmail);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }
    }
}
