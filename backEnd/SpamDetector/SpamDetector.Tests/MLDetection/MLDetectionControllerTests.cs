﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SpamDetector.Controllers;
using SpamDetector.Features.MLDetection.FeedBack.Commands.AddFeeback;
using SpamDetector.Features.MLDetection.FeedBack.Dtos;
using SpamDetector.Models.MLDetection;
using SpamDetector.Features.MLDetection.Attack.Commands.AddAttack;
using SpamDetector.Features.MLDetection.Attack.Queries.GetAttackPeriodsByUser;
using SpamDetector.Features.MLDetection.Attack.Queries.GetBiggestAttackerByUser;
using SpamDetector.Features.MLDetection.FeedBack.Queries.GetFeedbackByMessage;
using SpamDetector.Features.MLDetection.Prediction.Queries;
using SpamDetector.Features.MLDetection.Training.Queries;


namespace SpamDetector.Tests.MLDetection
{
    public class MLDetectionControllerTests
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly MLDetectionController _controller;

        public MLDetectionControllerTests()
        {
            _mediatorMock = new Mock<IMediator>();
            _controller = new MLDetectionController(_mediatorMock.Object);
        }

        [Fact]
        public async Task MessagePrediction_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var requestModel = new PredictionRequestModelWithUser();
            _mediatorMock.Setup(m => m.Send(It.IsAny<PredictionQuery>(), default)).ReturnsAsync(new ModelOutput());

            // Act
            var result = await _controller.MessagePrediction(requestModel);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task MessagePrediction_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var requestModel = new PredictionRequestModelWithUser();
            _mediatorMock.Setup(m => m.Send(It.IsAny<PredictionQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.MessagePrediction(requestModel);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task TrainModel_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            _mediatorMock.Setup(m => m.Send(It.IsAny<TrainingQuery>(), default)).ReturnsAsync(true);

            // Act
            var result = await _controller.TrainModel();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task TrainModel_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            _mediatorMock.Setup(m => m.Send(It.IsAny<TrainingQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.TrainModel();

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task SaveAttack_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var saveAttackRequestModel = new SaveAttackRequestModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddAttackCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.SaveAttack(saveAttackRequestModel);

            // Assert
            var okResult = Assert.IsType<OkResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task SaveAttack_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var saveAttackRequestModel = new SaveAttackRequestModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddAttackCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.SaveAttack(saveAttackRequestModel);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task AttackPeriods_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var email = "Fact@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetAttackPeriodsByUserQuery>(), default)).ReturnsAsync(new List<Tuple<string, int, int>>());

            // Act
            var result = await _controller.AttackPeriods(email);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task AttackPeriods_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var email = "Fact@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetAttackPeriodsByUserQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.AttackPeriods(email);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task BiggestThreath_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var email = "Fact@example.com";
            var biggestTherath =
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetBiggestAttackerByUserQuery>(), default)).ReturnsAsync(new List<Tuple<string, string, int>>());

            // Act
            var result = await _controller.BiggestThreath(email);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task BiggestThreath_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var email = "Fact@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetBiggestAttackerByUserQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.BiggestThreath(email);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task AddFeedback_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var feedbackModelDto = new FeedBackModelDto();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddFeedbackCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.AddFeedback(feedbackModelDto);

            // Assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task AddFeedback_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var feedbackModelDto = new FeedBackModelDto();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddFeedbackCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.AddFeedback(feedbackModelDto);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }

        [Fact]
        public async Task GetFeedback_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var message = "Fact message";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetFeedbackByMessageQuery>(), default)).ReturnsAsync(true);

            // Act
            var result = await _controller.GetFeedback(message);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetFeedback_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var message = "Fact message";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetFeedbackByMessageQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.GetFeedback(message);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.Equal("Error", badRequestResult.Value);
        }
    }
}