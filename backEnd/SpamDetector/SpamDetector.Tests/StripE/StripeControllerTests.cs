﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SpamDetector.Controllers;
using SpamDetector.Features.StripE.SessioN.Commands.AddCheckoutSession;
using SpamDetector.Features.StripE.SessioN.Queries.GetSessionResponse;
using SpamDetector.Models.Stripe;
using System.Net;


namespace SpamDetector.Tests.StripE
{
    public class StripeControllerTests
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly StripeController _controller;

        public StripeControllerTests()
        {
            _mediatorMock = new Mock<IMediator>();
            _controller = new StripeController(_mediatorMock.Object);
        }

        [Fact]
        public async Task CreateCheckoutSession_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var createCheckoutSessionModel = new CreateCheckoutSessionModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddCheckoutSessionCommand>(), default)).ReturnsAsync(new CreateCheckoutSessionResponseModel());

            // Act
            var result = await _controller.CreateCheckoutSession(createCheckoutSessionModel) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.IsType<CreateCheckoutSessionResponseModel>(result.Value);
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task CreateCheckoutSession_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var createCheckoutSessionModel = new CreateCheckoutSessionModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<AddCheckoutSessionCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.CreateCheckoutSession(createCheckoutSessionModel) as BadRequestObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task GettSessionDetails_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var sessionDetailsRequestModel = new SessionDetailsRequestModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetSessionResponseQuery>(), default)).ReturnsAsync(true);

            // Act
            var result = await _controller.GettSessionDetails(sessionDetailsRequestModel) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.IsType<bool>(result.Value);
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task GettSessionDetails_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var sessionDetailsRequestModel = new SessionDetailsRequestModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetSessionResponseQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.GettSessionDetails(sessionDetailsRequestModel) as BadRequestObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
