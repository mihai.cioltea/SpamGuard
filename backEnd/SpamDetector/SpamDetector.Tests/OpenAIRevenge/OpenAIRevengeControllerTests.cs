﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using OpenAI_API.Completions;
using SpamDetector.Controllers;
using SpamDetector.Features.OpenAIRevenge.Commands.RemoveActionTokenRevenge;
using SpamDetector.Features.OpenAIRevenge.Commands.SendAttack;
using SpamDetector.Features.OpenAIRevenge.Queries.GetAIResponse;
using SpamDetector.Models.OpenAIRevenge;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace SpamDetector.Tests.OpenAIRevenge
{
    public class OpenAIRevengeControllerTests
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly OpenAIRevengeController _controller;

        public OpenAIRevengeControllerTests()
        {
            _mediatorMock = new Mock<IMediator>();
            _controller = new OpenAIRevengeController(_mediatorMock.Object);
        }

        [Fact]
        public async Task GenerateConversation_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var generatedMessage = "";
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetAIResponseQuery>(), default)).ReturnsAsync(generatedMessage);

            // Act
            var result = await _controller.CreateCheckoutSession() as OkObjectResult;

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnedCompletionResult = Assert.IsType<string>(okResult.Value);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);
        }

        [Fact]
        public async Task GenerateConversation_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            _mediatorMock.Setup(m => m.Send(It.IsAny<GetAIResponseQuery>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.CreateCheckoutSession() as BadRequestObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task RemoveActionTokenHasRevenge_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var userEmail = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<RemoveActionTokenRevengeCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.RemoveActionTokenHasRevenge(userEmail) as OkResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task RemoveActionTokenHasRevenge_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var userEmail = "test@example.com";
            _mediatorMock.Setup(m => m.Send(It.IsAny<RemoveActionTokenRevengeCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.RemoveActionTokenHasRevenge(userEmail) as BadRequestObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task SendAttack_ValidRequest_ReturnsOkResult()
        {
            // Arrange
            var revengeModel = new RevengeModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<SendAttackCommand>(), default)).Returns(Task.CompletedTask);

            // Act
            var result = await _controller.SendAttack(revengeModel) as OkResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task SendAttack_InvalidRequest_ReturnsBadRequest()
        {
            // Arrange
            var revengeModel = new RevengeModel();
            _mediatorMock.Setup(m => m.Send(It.IsAny<SendAttackCommand>(), default)).ThrowsAsync(new Exception("Error"));

            // Act
            var result = await _controller.SendAttack(revengeModel) as BadRequestObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal((int)HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
