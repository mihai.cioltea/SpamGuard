﻿using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using Microsoft.ML;
using SpamDetector.Models.MLDetection;
using Microsoft.Data.SqlClient;

namespace SpamDetector
{
    public partial class EmailSpamDetector
    {
        private readonly IConfiguration _configuration;

        public EmailSpamDetector(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public static ITransformer RetrainPipeline(MLContext mlContext, IDataView trainData)
        {
            var pipeline = BuildPipeline(mlContext);
            var model = pipeline.Fit(trainData);

            return model;
        }

        public async Task<Boolean> TrainAndSaveModel()
        {

            MLContext mlContext = new MLContext();
            string sqlCommand = "SELECT Category,Message FROM SpamDataSet";
            string connectionString = _configuration.GetSection("ConnectionStrings:DefaultConnection").Value!;
            DatabaseSource dbSource = new DatabaseSource(SqlClientFactory.Instance, connectionString, sqlCommand);
            DatabaseLoader loader = mlContext.Data.CreateDatabaseLoader<ModelInput>();
            IDataView data = loader.Load(dbSource);

            var model = RetrainPipeline(mlContext, data);
            mlContext.Model.Save(model, data.Schema, MLNetModelPath);

            return true;
        }

        public static IEstimator<ITransformer> BuildPipeline(MLContext mlContext)
        {
            // Data process configuration with pipeline data transformations
            var pipeline = mlContext.Transforms.Text.FeaturizeText(inputColumnName:@"Message",outputColumnName:@"Message")      
                                    .Append(mlContext.Transforms.Concatenate(@"Features", new []{@"Message"}))      
                                    .Append(mlContext.Transforms.Conversion.MapValueToKey(outputColumnName:@"Category",inputColumnName:@"Category"))      
                                    .Append(mlContext.Transforms.NormalizeMinMax(@"Features", @"Features"))      
                                    .Append(mlContext.MulticlassClassification.Trainers.LbfgsMaximumEntropy(new LbfgsMaximumEntropyMulticlassTrainer.Options(){L1Regularization=0.04771658F,L2Regularization=4.517905F,LabelColumnName=@"Category",FeatureColumnName=@"Features"}))      
                                    .Append(mlContext.Transforms.Conversion.MapKeyToValue(outputColumnName:@"PredictedLabel",inputColumnName:@"PredictedLabel"));

            return pipeline;
        }
    }
}
