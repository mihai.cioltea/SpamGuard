﻿using MediatR;
using OpenAI_API;
using OpenAI_API.Completions;
using SpamDetector.Data;

namespace SpamDetector.Features.OpenAIRevenge.Queries.GetAIResponse
{
    public class GetAIResponseQueryHandler : IRequestHandler<GetAIResponseQuery, string>
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;
        public GetAIResponseQueryHandler(DataContext dataContext, IConfiguration configuration)
        {
            _dataContext = dataContext;
            _configuration = configuration;
        }

        public async Task<string> Handle(GetAIResponseQuery request, CancellationToken cancellationToken)
        {
            var openAiApiKey = _configuration.GetSection("OpenAI:SecretKey").Value!;

            var openAIApi = new OpenAIAPI(openAiApiKey);
            var parameters = new CompletionRequest
            {
                Model = "gpt-3.5-turbo-instruct",
                Prompt = "Generate a clean email that cannot be recognised as spam.",
                Temperature = 0.9,
                MaxTokens = 1000
            };

            var response = await openAIApi.Completions.CreateCompletionsAsync(parameters);
            return response.Completions[0].Text; 
        }
    }
}
