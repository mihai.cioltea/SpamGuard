﻿using MediatR;

namespace SpamDetector.Features.OpenAIRevenge.Queries.GetAIResponse
{
    public class GetAIResponseQuery : IRequest<string>
    {
    }
}
