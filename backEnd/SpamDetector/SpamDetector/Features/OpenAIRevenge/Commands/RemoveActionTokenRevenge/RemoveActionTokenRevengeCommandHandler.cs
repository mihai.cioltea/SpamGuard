﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.OpenAIRevenge.Commands.RemoveActionTokenRevenge
{
    public class RemoveActionTokenRevengeCommandHandler : IRequestHandler<RemoveActionTokenRevengeCommand>
    {
        private readonly DataContext _dataContext;
        public RemoveActionTokenRevengeCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task Handle(RemoveActionTokenRevengeCommand request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.UserEmail, cancellationToken);

            if(isInDb is not null)
            {
                isInDb.HasRevengeAction = false;
                _dataContext.ActionTokens.Update(isInDb);
                await _dataContext.SaveChangesAsync(cancellationToken);
            }
        }
    }
}
