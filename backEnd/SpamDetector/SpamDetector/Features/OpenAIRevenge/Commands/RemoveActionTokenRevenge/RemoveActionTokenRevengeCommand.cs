﻿using MediatR;

namespace SpamDetector.Features.OpenAIRevenge.Commands.RemoveActionTokenRevenge
{
    public class RemoveActionTokenRevengeCommand : IRequest
    {
        public string UserEmail { get; set; }
    }
}
