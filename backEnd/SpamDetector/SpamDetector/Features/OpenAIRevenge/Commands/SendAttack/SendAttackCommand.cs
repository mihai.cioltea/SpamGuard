﻿using MediatR;
using SpamDetector.Models.OpenAIRevenge;

namespace SpamDetector.Features.OpenAIRevenge.Commands.SendAttack
{
    public class SendAttackCommand : IRequest
    {
        public RevengeModel RevengeModel { get; set; }
    }
}
