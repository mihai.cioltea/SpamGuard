﻿using MediatR;
using SpamDetector.HelpfulServices.EmailSenderService;

namespace SpamDetector.Features.OpenAIRevenge.Commands.SendAttack
{
    public class SendAttackCommandHandler : IRequestHandler<SendAttackCommand>
    {
        private readonly IEmailSenderService _emailSenderService;
        public SendAttackCommandHandler(IEmailSenderService emailSenderService)
        {
            _emailSenderService = emailSenderService;
        }

        public async Task Handle(SendAttackCommand request, CancellationToken cancellationToken)
        {
            _emailSenderService.SendRevengeEmail(request.RevengeModel);
        }
    }
}
