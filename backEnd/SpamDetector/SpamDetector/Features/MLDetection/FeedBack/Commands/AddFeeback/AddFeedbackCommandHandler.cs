﻿using MediatR;
using SpamDetector.Data;
using SpamDetector.Models.MLDetection;

namespace SpamDetector.Features.MLDetection.FeedBack.Commands.AddFeeback
{
    public class AddFeedbackCommandHandler : IRequestHandler<AddFeedbackCommand>
    {
        private readonly DataContext _dataContext;
        public AddFeedbackCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task Handle(AddFeedbackCommand request, CancellationToken cancellationToken)
        {
            var feedbackModel = new FeedbackModel();
            feedbackModel.Message = request.FeedBackModel.Message;
            feedbackModel.IsOK = request.FeedBackModel.IsOK;

            await _dataContext.Feedbacks.AddAsync(feedbackModel, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
        }
    }
}
