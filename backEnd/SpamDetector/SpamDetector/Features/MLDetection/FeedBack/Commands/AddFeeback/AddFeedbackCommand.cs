﻿using MediatR;
using SpamDetector.Features.MLDetection.FeedBack.Dtos;

namespace SpamDetector.Features.MLDetection.FeedBack.Commands.AddFeeback
{
    public class AddFeedbackCommand : IRequest
    {
        public FeedBackModelDto FeedBackModel { get; set; }
    }
}
