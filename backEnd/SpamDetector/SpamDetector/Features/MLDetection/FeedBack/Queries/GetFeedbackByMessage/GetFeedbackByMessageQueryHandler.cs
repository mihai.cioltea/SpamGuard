﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.MLDetection.FeedBack.Queries.GetFeedbackByMessage
{
    public class GetFeedbackByMessageQueryHandler : IRequestHandler<GetFeedbackByMessageQuery, bool>
    {
        private readonly DataContext _dataContext;
        public GetFeedbackByMessageQueryHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(GetFeedbackByMessageQuery request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.Feedbacks
                .FirstOrDefaultAsync(fb => fb.Message == request.Message, cancellationToken);

            if (isInDb is null)
            {
                return false;
            }
            return true;
        }
    }
}
