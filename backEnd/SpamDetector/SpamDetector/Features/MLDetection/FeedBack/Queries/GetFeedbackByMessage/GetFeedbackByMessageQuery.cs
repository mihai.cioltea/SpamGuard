﻿using MediatR;

namespace SpamDetector.Features.MLDetection.FeedBack.Queries.GetFeedbackByMessage
{
    public class GetFeedbackByMessageQuery : IRequest<bool>
    {
        public string Message { get; set; }
    }
}
