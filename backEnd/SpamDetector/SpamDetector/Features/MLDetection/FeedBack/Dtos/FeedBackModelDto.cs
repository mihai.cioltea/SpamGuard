﻿using SpamDetector.Models.MLDetection;

namespace SpamDetector.Features.MLDetection.FeedBack.Dtos
{
    public class FeedBackModelDto
    {
        public bool IsOK { get; set; }
        public string Message { get; set; }

        public static explicit operator FeedbackModel(FeedBackModelDto feedBackModelDto)
        {
            return new()
            {
                IsOK = feedBackModelDto.IsOK,
                Message = feedBackModelDto.Message,
            };
        }
    }
}
