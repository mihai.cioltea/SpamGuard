﻿using MediatR;
using SpamDetector.Models.MLDetection;

namespace SpamDetector.Features.MLDetection.Prediction.Queries
{
    public class PredictionQuery : IRequest<ModelOutput>
    {
        public PredictionRequestModel PredictionRequestModel { get; set; }
        public string UserEmail { get; set; }
    }
}
