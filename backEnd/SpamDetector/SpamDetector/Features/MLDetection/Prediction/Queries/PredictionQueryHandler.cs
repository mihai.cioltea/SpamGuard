﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;
using SpamDetector.Models.MLDetection;

namespace SpamDetector.Features.MLDetection.Prediction.Queries
{
    public class PredictionQueryHandler : IRequestHandler<PredictionQuery, ModelOutput>
    {
        private readonly DataContext _dataContext;
        public PredictionQueryHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<ModelOutput> Handle(PredictionQuery request, CancellationToken cancellationToken)
        {
            if (request.PredictionRequestModel.MailBody.Equals(string.Empty))
            {
                throw new Exception("Email message cannot be empty");
            }

            var isUserInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.UserEmail, cancellationToken);

            if (isUserInDb!.NoOfChecks == 0)
            {
                throw new Exception("You have reached the maximum amount of checks for today!");
            }

            ModelInput modelInput = new ModelInput()
            {
                Message = request.PredictionRequestModel.MailBody
            };

            return await EmailSpamDetector.Predict(modelInput);
        }
    }
}
