﻿using MediatR;

namespace SpamDetector.Features.MLDetection.Training.Queries
{
    public class TrainingQueryHandler : IRequestHandler<TrainingQuery, bool>
    {
        private readonly IConfiguration _configuration;
        public TrainingQueryHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<bool> Handle(TrainingQuery request, CancellationToken cancellationToken)
        {
            var emailDetector = new EmailSpamDetector(_configuration);
            return await emailDetector.TrainAndSaveModel();
        }
    }
}
