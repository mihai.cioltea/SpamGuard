﻿using MediatR;

namespace SpamDetector.Features.MLDetection.Training.Queries
{
    public class TrainingQuery : IRequest<bool>
    {
    }
}
