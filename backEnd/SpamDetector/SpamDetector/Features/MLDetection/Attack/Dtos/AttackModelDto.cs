﻿using SpamDetector.Models.MLDetection;

namespace SpamDetector.Features.MLDetection.Attack.Dtos
{
    public class AttackModelDto
    {
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Date { get; set; }
        public string UserEmailProvider { get; set; }
        public Boolean IsSpam { get; set; }

        public static explicit operator AttackModel(AttackModelDto attackDto)
        {
            return new()
            {
                From = attackDto.From,
                To = attackDto.To,
                Date = attackDto.Date,
                UserEmailProvider = attackDto.UserEmailProvider,
                IsSpam = attackDto.IsSpam,
            };
        }
    }
}
