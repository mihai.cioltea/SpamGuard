﻿using MediatR;
using SpamDetector.Features.MLDetection.Attack.Dtos;

namespace SpamDetector.Features.MLDetection.Attack.Commands.AddAttack
{
    public class AddAttackCommand : IRequest
    {
        public AttackModelDto Attack { get; set; }
        public string UserEmail { get; set; }
    }
}
