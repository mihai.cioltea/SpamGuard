﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;
using SpamDetector.Models.MLDetection;

namespace SpamDetector.Features.MLDetection.Attack.Commands.AddAttack
{
    public class AddAttackCommandHandler : IRequestHandler<AddAttackCommand>
    {
        private readonly DataContext _dataContext;
        public AddAttackCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task Handle(AddAttackCommand request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.Attacks
                .FirstOrDefaultAsync(a => 
                    a.From == request.Attack.From &&
                    a.To == request.Attack.To &&
                    a.UserEmailProvider == request.Attack.UserEmailProvider &&
                    a.Date == request.Attack.Date, cancellationToken);

            if (isInDb is not null)
            {
                throw new Exception("The attack is already stored!");
            }

            var isUserInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.UserEmail, cancellationToken);
            
            if(isUserInDb!.NoOfChecks > 0)
            {
                isUserInDb!.NoOfChecks--;
                _dataContext.ActionTokens.Update(isUserInDb);
                await _dataContext.SaveChangesAsync(cancellationToken);
            }

            if(isUserInDb.NoOfChecks == 0)
            {
                throw new Exception("You have reached the maximum amount of checks for today!");
            }

            AttackModel attackModel = (AttackModel)request.Attack;

            await _dataContext.Attacks.AddAsync(attackModel, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
        }
    }
}
