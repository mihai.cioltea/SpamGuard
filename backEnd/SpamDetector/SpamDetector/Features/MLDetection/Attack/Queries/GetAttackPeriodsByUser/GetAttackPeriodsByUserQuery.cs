﻿using MediatR;

namespace SpamDetector.Features.MLDetection.Attack.Queries.GetAttackPeriodsByUser
{
    public class GetAttackPeriodsByUserQuery : IRequest<List<Tuple<string, int, int>>>
    {
        public string Email { get; set; }
    }
}
