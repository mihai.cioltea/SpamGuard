﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.MLDetection.Attack.Queries.GetAttackPeriodsByUser
{
    public class GetAttackPeriodsByUserQueryHandler : IRequestHandler<GetAttackPeriodsByUserQuery, List<Tuple<string, int, int>>>
    {
        private readonly DataContext _dataContext;
        public GetAttackPeriodsByUserQueryHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<Tuple<string, int, int>>> Handle(GetAttackPeriodsByUserQuery request, CancellationToken cancellationToken)
        {
            var queryAttacks = await _dataContext.Attacks
                .Where(a => a.To == request.Email)
                .GroupBy(a => a.Date.Date)
                .Select(g => new
                    {
                        Date = g.Key,
                        IsHam = g.Sum(a => a.IsSpam ? 0 : 1),
                        IsSpam = g.Sum(a => a.IsSpam ? 1 : 0)
                    }).ToListAsync(cancellationToken);

            var result = queryAttacks.Select(x => 
                Tuple.Create(
                    x.Date.ToString("dd-MM-yyyy"),
                    x.IsHam,
                    x.IsSpam)).ToList();

            return result;
        }
    }
}
