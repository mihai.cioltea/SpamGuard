﻿using MediatR;

namespace SpamDetector.Features.MLDetection.Attack.Queries.GetBiggestAttackerByUser
{
    public class GetBiggestAttackerByUserQuery : IRequest<List<Tuple<string, string, int>>>
    {
        public string Email { get; set; }
    }
}
