﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.MLDetection.Attack.Queries.GetBiggestAttackerByUser
{
    public class GetBiggestAttackerByUserQueryHandler : IRequestHandler<GetBiggestAttackerByUserQuery, List<Tuple<string, string, int>>>
    {
        private readonly DataContext _dataContext;
        public GetBiggestAttackerByUserQueryHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<List<Tuple<string, string, int>>> Handle(GetBiggestAttackerByUserQuery request, CancellationToken cancellationToken)
        {
            var queryAttacks = await _dataContext.Attacks
                .Where(a => a.To == request.Email && a.IsSpam)
                .GroupBy(a => new { Month = a.Date.Month, Year = a.Date.Year })
                .Select(g => new
                    {
                        Month = g.Key.Month,
                        Year = g.Key.Year,
                        TotalAttacks = g.Count(),
                        MostAttacksByAttacker = g.GroupBy(a => a.From)
                                                      .Select(ag => new
                                                      {
                                                          Attacker = ag.Key,
                                                          AttackCount = ag.Count()
                                                      })
                                                      .OrderByDescending(ag => ag.AttackCount)
                                                      .FirstOrDefault()
                    }).ToListAsync(cancellationToken);

            var result = queryAttacks.Select(x => Tuple.Create(
                new DateTime(x.Year, x.Month, 1).ToString("MM-yyyy"),
                x.MostAttacksByAttacker != null ? x.MostAttacksByAttacker.Attacker : "No attacks",
                x.TotalAttacks)).ToList();

            return result;
        }
    }
}
