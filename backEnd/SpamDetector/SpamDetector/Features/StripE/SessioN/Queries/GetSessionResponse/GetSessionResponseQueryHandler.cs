﻿using MediatR;
using SpamDetector.Features.StripE.SessioN.Commands.UpdateActionTokenOnPaid;
using SpamDetector.Features.StripE.SessioN.Queries.GetSessionResponse;
using Stripe;
using Stripe.Checkout;

namespace SpamDetector.Features.Stripe.SessioN.Queries.GetSessionResponse
{
    public class GetSessionResponseQueryHandler : IRequestHandler<GetSessionResponseQuery, bool>
    {
        private readonly IConfiguration _configuration;
        private readonly IMediator _mediator;
        public GetSessionResponseQueryHandler(IConfiguration configuration, IMediator mediator)
        {
            _configuration = configuration;
            _mediator = mediator;
        }

        public async Task<bool> Handle(GetSessionResponseQuery request, CancellationToken cancellationToken)
        {
            StripeConfiguration.ApiKey = _configuration.GetSection("Stripe:SecretKey").Value;
            var service = new SessionService();
            var session = service.Get(request.SesionId);

            if (session.PaymentStatus.Equals("paid"))
            {
                await _mediator.Send(new UpdateActionTokenOnPaidCommand() { UserEmail = request.UserEmail, PriceId = request.PriceId });
                return true;
            }

            return false;
        }
    }
}
