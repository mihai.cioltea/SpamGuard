﻿using MediatR;

namespace SpamDetector.Features.StripE.SessioN.Queries.GetSessionResponse
{
    public class GetSessionResponseQuery : IRequest<bool>
    {
        public string SesionId { get; set; }
        public string UserEmail { get; set; }
        public string PriceId { get; set; }
    }
}
