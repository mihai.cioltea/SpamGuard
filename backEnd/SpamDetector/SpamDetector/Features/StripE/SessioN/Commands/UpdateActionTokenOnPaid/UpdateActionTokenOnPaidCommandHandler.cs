﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.StripE.SessioN.Commands.UpdateActionTokenOnPaid
{
    public class UpdateActionTokenOnPaidCommandHandler : IRequestHandler<UpdateActionTokenOnPaidCommand>
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;
        public UpdateActionTokenOnPaidCommandHandler(DataContext dataContext, IConfiguration configuration)
        {
            _dataContext = dataContext;
            _configuration = configuration;
        }

        public async Task Handle(UpdateActionTokenOnPaidCommand request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.UserEmail, cancellationToken);

            if (isInDb is not null)
            {
                if(request.PriceId == _configuration.GetSection("Stripe:CHECKTOKENPRICEID").Value)
                {
                    isInDb.NoOfChecks += 15;
                    isInDb.Availability = DateTime.Today;
                }
                else
                {
                    isInDb.HasRevengeAction = true;
                }

                _dataContext.ActionTokens.Update(isInDb);
                await _dataContext.SaveChangesAsync(cancellationToken);
            }
        }
    }
}
