﻿using MediatR;

namespace SpamDetector.Features.StripE.SessioN.Commands.UpdateActionTokenOnPaid
{
    public class UpdateActionTokenOnPaidCommand : IRequest
    {
        public string UserEmail { get; set; }
        public string PriceId { get; set; }
    }
}
