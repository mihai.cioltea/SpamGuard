﻿using MediatR;
using SpamDetector.Models.Stripe;

namespace SpamDetector.Features.StripE.SessioN.Commands.AddCheckoutSession
{
    public class AddCheckoutSessionCommand : IRequest<CreateCheckoutSessionResponseModel>
    {
        public CreateCheckoutSessionModel SessionModel { get; set; }
    }
}
