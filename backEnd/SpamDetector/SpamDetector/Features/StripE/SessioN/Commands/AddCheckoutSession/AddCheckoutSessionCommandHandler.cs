﻿using MediatR;
using SpamDetector.Models.Stripe;
using Stripe;
using Stripe.Checkout;

namespace SpamDetector.Features.StripE.SessioN.Commands.AddCheckoutSession
{
    public class AddCheckoutSessionCommandHandler : IRequestHandler<AddCheckoutSessionCommand, CreateCheckoutSessionResponseModel>
    {
        private readonly IConfiguration _configuration;
        public AddCheckoutSessionCommandHandler(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<CreateCheckoutSessionResponseModel> Handle(AddCheckoutSessionCommand request, CancellationToken cancellationToken)
        {
            StripeConfiguration.ApiKey = _configuration.GetSection("Stripe:SecretKey").Value;
            var options = new SessionCreateOptions
            {
                SuccessUrl = request.SessionModel.SuccessUrl,
                CancelUrl = request.SessionModel.FailureUrl,
                AutomaticTax = new SessionAutomaticTaxOptions { Enabled = true },
                PaymentMethodTypes = new List<string>
                {
                    "card",
                },
                Mode = "payment",
                LineItems = new List<SessionLineItemOptions>
                {
                    new SessionLineItemOptions
                    {
                        Price = request.SessionModel.PriceId,
                        Quantity = 1,
                    },
                },
            };

            var service = new SessionService();
            service.Create(options);
            var session = await service.CreateAsync(options);
            return new CreateCheckoutSessionResponseModel
            {
                SessionId = session.Id,
                PublicKey = _configuration.GetSection("Stripe:PublishableKey").Value!
            };
        }
    }
}
