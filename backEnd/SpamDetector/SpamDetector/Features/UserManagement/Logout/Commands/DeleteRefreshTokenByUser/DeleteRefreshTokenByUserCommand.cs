﻿using MediatR;

namespace SpamDetector.Features.UserManagement.Logout.Commands.DeleteRefreshTokenByUser
{
    public class DeleteRefreshTokenByUserCommand : IRequest
    {
        public string UserEmail { get; set; }
    }
}
