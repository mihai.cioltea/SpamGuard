﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.UserManagement.Logout.Commands.DeleteRefreshTokenByUser
{
    public class DeleteRefreshTokenByUserCommandHandler : IRequestHandler<DeleteRefreshTokenByUserCommand>
    {
        private readonly DataContext _dataContext;
        public DeleteRefreshTokenByUserCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;

        }

        public async Task Handle(DeleteRefreshTokenByUserCommand request, CancellationToken cancellationToken)
        {
            var isTokenInDb = await _dataContext.RefreshTokens
                .FirstOrDefaultAsync(rt => rt.UserEmail == request.UserEmail, cancellationToken);

            if (isTokenInDb is null)
            {
                throw new Exception($"Refresh Token for user: {request.UserEmail} does not exists!");
            }

            _dataContext.RefreshTokens.Remove(isTokenInDb);
            await _dataContext.SaveChangesAsync(cancellationToken);
        }
    }
}
