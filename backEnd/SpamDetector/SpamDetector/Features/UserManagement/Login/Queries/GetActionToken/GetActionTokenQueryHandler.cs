﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;
using SpamDetector.Models.UserManagement;
using System.Runtime.CompilerServices;

namespace SpamDetector.Features.UserManagement.Login.Queries.GetActionToken
{
    public class GetActionTokenQueryHandler : IRequestHandler<GetActionTokenQuery, ActionToken>
    {
        private readonly DataContext _dataContext;
        public GetActionTokenQueryHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<ActionToken> Handle(GetActionTokenQuery request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.UserEmail, cancellationToken);

            return isInDb;
        }
    }
}
