﻿using MediatR;
using SpamDetector.Models.UserManagement;

namespace SpamDetector.Features.UserManagement.Login.Queries.GetActionToken
{
    public class GetActionTokenQuery : IRequest<ActionToken>
    {
        public string UserEmail { get; set; }
    }
}
