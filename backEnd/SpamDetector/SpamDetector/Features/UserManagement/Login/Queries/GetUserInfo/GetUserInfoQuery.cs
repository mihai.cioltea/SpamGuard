﻿using MediatR;

namespace SpamDetector.Features.UserManagement.Login.Queries.GetUserInfo
{
    public class GetUserInfoQuery : IRequest<string>
    {
        public string Email { get; set; }
    }
}
