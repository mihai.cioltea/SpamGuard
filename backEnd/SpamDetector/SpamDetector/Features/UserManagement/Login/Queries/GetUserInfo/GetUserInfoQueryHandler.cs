﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.UserManagement.Login.Queries.GetUserInfo
{
    public class GetUserInfoQueryHandler : IRequestHandler<GetUserInfoQuery, string>
    {
        private readonly DataContext _dataContext;
        public GetUserInfoQueryHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<string> Handle(GetUserInfoQuery request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.Users
                .FirstOrDefaultAsync(user => user.Email == request.Email);

            if (isInDb is null)
            {
                throw new Exception("The user does not exist!");
            }

            return isInDb.FirstName;
        }
    }
}
