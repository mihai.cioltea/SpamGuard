﻿using MediatR;
using SpamDetector.Models.UserManagement;

namespace SpamDetector.Features.UserManagement.Login.Commands.UpdateActionToken
{
    public class UpdateActionTokenCommand : IRequest
    {
        public User User { get; set; }
    }
}
