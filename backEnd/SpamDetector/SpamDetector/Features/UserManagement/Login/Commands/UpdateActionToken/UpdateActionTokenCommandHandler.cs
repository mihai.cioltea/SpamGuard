﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;

namespace SpamDetector.Features.UserManagement.Login.Commands.UpdateActionToken
{
    public class UpdateActionTokenCommandHandler : IRequestHandler<UpdateActionTokenCommand>
    {
        private readonly DataContext _dataContext;
        public UpdateActionTokenCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task Handle(UpdateActionTokenCommand request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.User.Email, cancellationToken);

            if (isInDb is null) 
            {
                return;
            }

            if(isInDb.Availability.Date != DateTime.Today)
            {
                if (isInDb.NoOfChecks == 0)
                {
                    isInDb.NoOfChecks = 5;
                }
                isInDb.Availability = DateTime.Today;
            }

            _dataContext.ActionTokens.Update(isInDb);
            await _dataContext.SaveChangesAsync(cancellationToken);
        }
    }
}
