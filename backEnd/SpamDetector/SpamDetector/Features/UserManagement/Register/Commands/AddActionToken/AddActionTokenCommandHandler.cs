﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SpamDetector.Data;
using SpamDetector.Models.UserManagement;

namespace SpamDetector.Features.UserManagement.Register.Commands.AddActionToken
{
    public class AddActionTokenCommandHandler : IRequestHandler<AddActionTokenCommand, bool>
    {
        private readonly DataContext _dataContext;
        public AddActionTokenCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> Handle(AddActionTokenCommand request, CancellationToken cancellationToken)
        {
            var isInDb = await _dataContext.ActionTokens
                .FirstOrDefaultAsync(at => at.UserEmail == request.User.Email, cancellationToken);

            if(isInDb is not null)
            {
                throw new Exception("There is an Action Token for the user with email: "+ request.User.Email);
            }

            var actionToken = new ActionToken()
            {
                UserEmail = request.User.Email,
                NoOfChecks = 5,
                HasRevengeAction = false,
                Availability = DateTime.Today
            };

            await _dataContext.ActionTokens.AddAsync(actionToken, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}
