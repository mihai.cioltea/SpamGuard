﻿using MediatR;
using SpamDetector.Features.UserManagement.Register.Dtos;

namespace SpamDetector.Features.UserManagement.Register.Commands.AddActionToken
{
    public class AddActionTokenCommand : IRequest<bool>
    {
        public UserRegisterDto User { get; set; }
    }
}
