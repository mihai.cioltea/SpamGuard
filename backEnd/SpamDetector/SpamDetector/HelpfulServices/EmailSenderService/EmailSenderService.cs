﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using SpamDetector.Features.UserManagement.Register.Dtos;
using SpamDetector.Models.UserManagement;
using MediatR;
using SpamDetector.Models.OpenAIRevenge;

namespace SpamDetector.HelpfulServices.EmailSenderService
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly IConfiguration _configuration;
        private readonly IMediator _mediator;

        public EmailSenderService(IConfiguration configuration, IMediator mediator)
        {
            _configuration = configuration;
            _mediator = mediator;
        }

        public bool SendWelcomeEmail(UserRegisterDto user)
        {
            try
            {
                var email = CreateWelcomeMailStructure(user);
                SendEmail(email);
                return true;
            }
            catch (SmtpCommandException ex)
            {
                return false;
            }
        }

        public bool SendResetPasswordEmail(UserPasswordReset user)
        {
            try
            {
                var email = CreateResetPasswordMailStructure(user);
                SendEmail(email);
                return true;
            }
            catch (SmtpCommandException ex)
            {
                return false;
            }
        }

        public async Task<bool> SendForgotPasswordEmail(string userEmail)
        {
            try
            {
                var email = await CreateForgotPasswordMailStructure(userEmail);
                await SendEmailAsync(email);
                return true;
            }
            catch (SmtpCommandException ex)
            {
                return false;
            }
        }

        public void SendRevengeEmail(RevengeModel revengeModel)
        {
            try
            {
                var email = CreateRevengeMailStructure(revengeModel);
                SendEmail(email);
            }
            catch (SmtpCommandException ex)
            {
            }
        }

        private void SendEmail(MimeMessage email)
        {
            using (var smtp = new SmtpClient())
            {
                smtp.Connect(_configuration.GetSection("SenderService:EmailHost").Value, 587, SecureSocketOptions.StartTls);
                smtp.Authenticate(_configuration.GetSection("SenderService:EmailUserName").Value, _configuration.GetSection("SenderService:EmailPassword").Value);
                smtp.Send(email);
                smtp.Disconnect(true);
            }
        }

        private async Task SendEmailAsync(MimeMessage email)
        {
            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync(_configuration.GetSection("SenderService:EmailHost").Value, 587, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(_configuration.GetSection("SenderService:EmailUserName").Value, _configuration.GetSection("SenderService:EmailPassword").Value);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
        }

        private MimeMessage CreateWelcomeMailStructure(UserRegisterDto user)
        {
            var emailBody = new EmailBody(_mediator, user);
            return ConfigureEmail(user.Email, "Welcome to Spam Guard!", emailBody.GetWelcomeEmailBody());
        }

        private MimeMessage CreateResetPasswordMailStructure(UserPasswordReset user)
        {
            var emailBody = new EmailBody(_mediator, null, user);
            return ConfigureEmail(user.Email, "The account credentials have been updated Spam Guard!", emailBody.GetResetPasswordBody());
        }

        private async Task<MimeMessage> CreateForgotPasswordMailStructure(string userEmail)
        {
            var emailBody = new EmailBody(_mediator);
            var body = await emailBody.GetForgotPasswordBody(userEmail);
            return ConfigureEmail(userEmail, "Did you forget your password?", body);
        }

        private MimeMessage CreateRevengeMailStructure(RevengeModel revengeModel)
        {
            var emailBody = new EmailBody(_mediator, null, null, revengeModel);
            return ConfigureEmail(revengeModel.AttackerEmail, "Enjoy your Day :)!", emailBody.GetRevengeEmailBody());
        }

        private MimeMessage ConfigureEmail(string to, string subject, string body)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("SenderService:EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Html) { Text = body };
            return email;
        }
    }
}
