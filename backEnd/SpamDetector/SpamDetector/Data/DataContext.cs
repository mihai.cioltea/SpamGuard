﻿using Microsoft.EntityFrameworkCore;
using SpamDetector.Models.MLDetection;
using SpamDetector.Models.UserManagement;

namespace SpamDetector.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<PasswordResetToken> PasswordResetTokens { get; set; }
        public DbSet<AttackModel> Attacks { get; set; }
        public DbSet<ActionToken> ActionTokens { get; set; }
        public DbSet<FeedbackModel> Feedbacks { get; set; }
    }
}
