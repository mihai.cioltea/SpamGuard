﻿using System.ComponentModel.DataAnnotations;

namespace SpamDetector.Models.Stripe
{
    public class CreateCheckoutSessionModel
    {
        [Required]
        public string PriceId { get; set; }
        [Required]
        public string SuccessUrl { get; set; }
        [Required]
        public string FailureUrl { get; set; }
    }
}
