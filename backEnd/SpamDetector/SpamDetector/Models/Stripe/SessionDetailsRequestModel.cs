﻿namespace SpamDetector.Models.Stripe
{
    public class SessionDetailsRequestModel
    {
        public string SessionId { get; set; }
        public string UserEmail { get; set; }

        public string PriceId { get; set; }
    }
}
