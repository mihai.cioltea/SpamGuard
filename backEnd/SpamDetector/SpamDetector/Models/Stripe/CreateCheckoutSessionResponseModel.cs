﻿namespace SpamDetector.Models.Stripe
{
    public class CreateCheckoutSessionResponseModel
    {
        public string SessionId { get; set; }
        public string PublicKey { get; set; }
    }
}
