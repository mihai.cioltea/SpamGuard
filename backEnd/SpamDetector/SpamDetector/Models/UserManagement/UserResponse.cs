﻿namespace SpamDetector.Models.UserManagement
{
    public class UserResponse
    {
        public string AuthTOKEN { get; set; }
        public string RefreshTOKEN { get; set; }
    }
}
