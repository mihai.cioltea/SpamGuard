﻿namespace SpamDetector.Models.UserManagement
{
    public class ActionToken
    {
        public int Id { get; set; }
        public int NoOfChecks { get; set; } = 5;
        public DateTime Availability { get; set; } = DateTime.Today;
        public bool HasRevengeAction { get; set; }
        public string UserEmail { get; set; }
        public User User { get; set; } 
    }
}
