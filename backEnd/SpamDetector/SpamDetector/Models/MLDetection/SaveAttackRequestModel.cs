﻿using SpamDetector.Features.MLDetection.Attack.Dtos;

namespace SpamDetector.Models.MLDetection
{
    public class SaveAttackRequestModel
    {
        public AttackModelDto AttackModel { get; set; }
        public string UserEmail { get; set; }
    }
}
