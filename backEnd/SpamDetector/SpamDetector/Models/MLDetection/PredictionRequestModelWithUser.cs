﻿namespace SpamDetector.Models.MLDetection
{
    public class PredictionRequestModelWithUser
    {
        public PredictionRequestModel PredictionModel { get; set; }
        public string UserEmail { get; set; }
    }
}
