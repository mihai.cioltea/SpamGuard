﻿namespace SpamDetector.Models.MLDetection
{
    public class PredictionRequestModel
    {
        public string Sender { get; set; }
        public string To { get; set; }
        public string MailBody { get; set; }
        public string Date { get; set; }
    }
}
