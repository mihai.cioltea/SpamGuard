﻿namespace SpamDetector.Models.MLDetection
{
    public class AttackModel
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime Date { get; set; }
        public string UserEmailProvider { get; set; }
        public Boolean IsSpam { get; set; }
    }
}
