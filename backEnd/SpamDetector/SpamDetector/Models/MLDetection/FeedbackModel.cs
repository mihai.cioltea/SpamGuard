﻿namespace SpamDetector.Models.MLDetection
{
    public class FeedbackModel
    {
        public int Id { get; set; }
        public bool IsOK { get; set; }
        public string Message { get; set; }
    }
}
