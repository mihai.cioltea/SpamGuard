﻿using Microsoft.ML.Data;

namespace SpamDetector.Models.MLDetection
{
    public class ModelOutput
    {
        [ColumnName(@"Category")]
        public uint Category { get; set; }

        [ColumnName(@"PredictedLabel")]
        public string PredictedLabel { get; set; }

        [ColumnName(@"Score")]
        public float[] Score { get; set; }
    }
}
