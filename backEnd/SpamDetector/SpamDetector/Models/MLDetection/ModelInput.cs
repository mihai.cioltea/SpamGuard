﻿using Microsoft.ML.Data;

namespace SpamDetector.Models.MLDetection
{
    public class ModelInput
    {
        [ColumnName(@"Category")]
        public string Category { get; set; }
        [ColumnName(@"Message")]
        public string Message { get; set; }
    }
}
