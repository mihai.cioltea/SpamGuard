﻿namespace SpamDetector.Models.OpenAIRevenge
{
    public class RevengeModel
    {
        public string UserEmail { get; set; }
        public string AttackerEmail { get; set; }
        public string GeneratedEmail { get; set; }
    }
}
