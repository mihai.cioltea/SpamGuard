﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenAI_API.Completions;
using SpamDetector.Features.OpenAIRevenge.Commands.RemoveActionTokenRevenge;
using SpamDetector.Features.OpenAIRevenge.Commands.SendAttack;
using SpamDetector.Features.OpenAIRevenge.Queries.GetAIResponse;
using SpamDetector.Models.OpenAIRevenge;
using System.Net;

namespace SpamDetector.Controllers
{
    [Route("api/OpenAIRevenge")]
    [ApiController]
    [Authorize]
    public class OpenAIRevengeController : ControllerBase
    {
        private readonly IMediator _mediatR;

        public OpenAIRevengeController(IMediator mediatR)
        {
            _mediatR = mediatR;
        }

        [HttpGet("generateConversation"), AllowAnonymous]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateCheckoutSession()
        {
            try
            {
                var response = await _mediatR.Send(new GetAIResponseQuery());
                return Ok(response);
            }
            catch (Exception ex)
            {  
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("removeHasRevenge"),  AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> RemoveActionTokenHasRevenge([FromBody] string userEmail)
        {
            try
            {
                await _mediatR.Send(new RemoveActionTokenRevengeCommand() { UserEmail = userEmail });
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("sendAttack"), AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SendAttack([FromBody] RevengeModel revengeModel)
        {
            try
            {
                await _mediatR.Send(new SendAttackCommand() { RevengeModel = revengeModel });
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
