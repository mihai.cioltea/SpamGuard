﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpamDetector.Features.MLDetection.Attack.Commands.AddAttack;
using SpamDetector.Features.MLDetection.Attack.Queries.GetAttackPeriodsByUser;
using SpamDetector.Features.MLDetection.Attack.Queries.GetBiggestAttackerByUser;
using SpamDetector.Features.MLDetection.FeedBack.Commands.AddFeeback;
using SpamDetector.Features.MLDetection.FeedBack.Dtos;
using SpamDetector.Features.MLDetection.FeedBack.Queries.GetFeedbackByMessage;
using SpamDetector.Features.MLDetection.Prediction.Queries;
using SpamDetector.Features.MLDetection.Training.Queries;
using SpamDetector.Models.MLDetection;
using System.Net;

namespace SpamDetector.Controllers
{
    [Route("api/MLDetection")]
    [ApiController]
    [Authorize]
    public class MLDetectionController : ControllerBase
    {
        private readonly IMediator _mediatR;
        public MLDetectionController(IMediator mediatR)
        {
            _mediatR = mediatR;
        }

        [HttpPost("messagePrediction"), AllowAnonymous]
        [ProducesResponseType(typeof(ModelOutput), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> MessagePrediction([FromBody] PredictionRequestModelWithUser predictionRequestModel)
        {
            try
            {
                var response = await _mediatR.Send(new PredictionQuery() {
                    PredictionRequestModel = predictionRequestModel.PredictionModel,
                    UserEmail = predictionRequestModel.UserEmail
                });
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("trainModel"), AllowAnonymous]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> TrainModel()
        {
            try
            {
                var response = await _mediatR.Send(new TrainingQuery());
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("saveAttack"), AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult> SaveAttack([FromBody] SaveAttackRequestModel saveAttackRequestModel)
        {
            try
            {
                await _mediatR.Send(new AddAttackCommand() { Attack = saveAttackRequestModel.AttackModel, UserEmail = saveAttackRequestModel.UserEmail });
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("attackPeriods"), AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult> AttackPeriods([FromQuery] string email)
        {
            try
            {
                var response = await _mediatR.Send(new GetAttackPeriodsByUserQuery() { Email = email });
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("biggestThreath"), AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult> BiggestThreath([FromQuery] string email)
        {
            try
            {
                var response = await _mediatR.Send(new GetBiggestAttackerByUserQuery() { Email = email });
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("addFeedback"), AllowAnonymous]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult> AddFeedback([FromBody] FeedBackModelDto feedBackModelDto)
        {
            try
            {
                await _mediatR.Send(new AddFeedbackCommand() { FeedBackModel = feedBackModelDto });
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost("getFeedback"), AllowAnonymous]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<ActionResult> GetFeedback([FromBody] string message)
        {
            try
            {
                var response = await _mediatR.Send(new GetFeedbackByMessageQuery() { Message = message });
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
