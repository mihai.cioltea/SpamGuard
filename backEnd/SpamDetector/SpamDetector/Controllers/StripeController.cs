﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpamDetector.Features.StripE.SessioN.Commands.AddCheckoutSession;
using SpamDetector.Features.StripE.SessioN.Queries.GetSessionResponse;
using SpamDetector.Models.MLDetection;
using SpamDetector.Models.Stripe;
using System.Net;

namespace SpamDetector.Controllers
{
    [Route("api/Stripe")]
    [ApiController]
    [Authorize]
    public class StripeController : ControllerBase
    {
        private readonly IMediator _mediatR;

        public StripeController(IMediator mediatR)
        {
            _mediatR = mediatR;
        }

        [HttpPost("create-checkout-session"), AllowAnonymous]
        [ProducesResponseType(typeof(CreateCheckoutSessionResponseModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateCheckoutSession([FromBody] CreateCheckoutSessionModel createCheckoutSessionModel)
        {
            try
            {
                var response = await _mediatR.Send(new AddCheckoutSessionCommand() { SessionModel = createCheckoutSessionModel });
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("session-details"), AllowAnonymous]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GettSessionDetails([FromBody] SessionDetailsRequestModel sessionDetailsRequestModel)
        {
            try
            {
                var response = await _mediatR.Send(new GetSessionResponseQuery() { SesionId = sessionDetailsRequestModel.SessionId, UserEmail = sessionDetailsRequestModel.UserEmail, PriceId = sessionDetailsRequestModel.PriceId });
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
